data "archive_file" "source_reminder_api" {
  type        = "zip"
  source_dir  = "${local.base_code_path}/../services/reminder/src/"
  output_path = "/tmp/reminder-${local.environment_name_slug}.zip"
  depends_on = [null_resource.services_npm_recursive_build ]
}
resource "aws_s3_bucket_object" "file_upload_reminder_api" {
  bucket = "frontend-dev-bucket-for-sam"
  key    = "reminder-${local.environment_name_slug}.zip"
  source = data.archive_file.source_reminder_api.output_path
}
resource "aws_cloudformation_stack" "deploy_reminder_api" {
  name = "${local.environment_name_slug}-reminder"
  capabilities = ["CAPABILITY_IAM", "CAPABILITY_AUTO_EXPAND"]
  on_failure = "DELETE"
  depends_on = [null_resource.services_npm_recursive_build ]
  parameters = {
        AppEnvironment = local.environment_name_slug
        ParamAWSID = var.aws_access_key_id
        ParamAWSKey = var.aws_secret_access_key
        ParamAWSRegion = var.aws_default_region
        ParamApiVersion = var.api_version_number
        ParamCognitoAppId = aws_cognito_user_pool_client.this.id
        ParamCognitoPool = aws_cognito_user_pool.this.id
        ParamDBHost = var.rds_db_host
        ParamDBPassword = var.rds_db_password
        ParamDBUsername = var.rds_db_username
        ParamEmailRedirectUrl = "${aws_s3_bucket.this_public.website_endpoint}/sign-up"
        ParamForgotPwdRedirect = "${aws_s3_bucket.this_public.website_endpoint}/reset-password"
        ParamS3PrivateBucket = aws_s3_bucket.this_private.id
        ParamS3PublicBucket = aws_s3_bucket.this_assets.id
        ParamVPCPrivateSubnet1 = var.vpc_private_subnet1
        ParamVPCPrivateSubnet2 = var.vpc_private_subnet2
        ParamVPCSG = var.vpc_sg
        ParamAWSRecipientEmail = var.recipient_email
        ParamAWSSenderEmail = var.sender_email
  }
  template_body = <<STACK
{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Transform": "AWS::Serverless-2016-10-31",
    "Description": "sx-mdc-service-reminder",
    "Parameters": {
        "AppEnvironment": {
            "Type": "String"
        },
        "ParamDBHost": {
            "Type": "String"
        },
        "ParamDBUsername": {
            "Type": "String"
        },
        "ParamDBPassword": {
            "Type": "String"
        },
        "ParamCognitoPool": {
            "Type": "String"
        },
        "ParamCognitoAppId": {
            "Type": "String"
        },
        "ParamEmailRedirectUrl": {
            "Type": "String"
        },
        "ParamForgotPwdRedirect": {
            "Type": "String"
        },
        "ParamS3PublicBucket": {
            "Type": "String"
        },
        "ParamS3PrivateBucket": {
            "Type": "String"
        },
        "ParamVPCPrivateSubnet1": {
            "Type": "String"
        },
        "ParamVPCPrivateSubnet2": {
            "Type": "String"
        },
        "ParamVPCSG": {
            "Type": "String"
        },
        "ParamAWSRegion": {
            "Type": "String"
        },
        "ParamAWSID": {
            "Type": "String"
        },
        "ParamAWSKey": {
            "Type": "String"
        },
        "ParamAWSSenderEmail": {
            "Type": "String"
        },
        "ParamAWSRecipientEmail": {
            "Type": "String"
        },
        "ParamApiVersion": {
            "Type": "String",
            "Default": "0.0.0"
        },
        "ParamIdTokenTtl": {
            "Type": "String",
            "Default": 1
        },
        "ParamAccessTokenTtl": {
            "Type": "String",
            "Default": 1
        },
        "ParamRefreshTokenTtl": {
            "Type": "String",
            "Default": 30
        }
    },
    "Globals": {
        "Function": {
            "Timeout": 10,
            "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
            "VpcConfig": {
                "SecurityGroupIds": [
                    {
                        "Ref": "ParamVPCSG"
                    }
                ],
                "SubnetIds": [
                    {
                        "Ref": "ParamVPCPrivateSubnet1"
                    },
                    {
                        "Ref": "ParamVPCPrivateSubnet2"
                    }
                ]
            },
            "Environment": {
                "Variables": {
                    "AWS_S3_ACCESS_KEY_ID": {
                        "Ref": "ParamAWSID"
                    },
                    "AWS_S3_SECRET_ACCESS_KEY": {
                        "Ref": "ParamAWSKey"
                    },
                    "AWS_S3_REGION": {
                        "Ref": "ParamAWSRegion"
                    },
                    "AWS_S3_PRIVATE_BUCKET": {
                        "Ref": "ParamS3PrivateBucket"
                    },
                    "AWS_S3_PUBLIC_BUCKET": {
                        "Ref": "ParamS3PublicBucket"
                    },
                    "POSTGRES_HOST": {
                        "Ref": "ParamDBHost"
                    },
                    "POSTGRES_USER": {
                        "Ref": "ParamDBUsername"
                    },
                    "POSTGRES_PWD": {
                        "Ref": "ParamDBPassword"
                    },
                    "PREFIX_COMMON_LIB": "/opt",
                    "API_VERSION": {
                        "Ref": "ParamApiVersion"
                    },
                    "MIGRATIONS_DIR": "/opt/rdb-migration/migrations",
                    "COMPANY_REGISTRY_TABLE_NAME": "company-registry-${local.environment_name_slug}",
                    "ID_TOKEN_TTL": {
                        "Ref": "ParamIdTokenTtl"
                    },
                    "ACCESS_TOKEN_TTL": {
                        "Ref": "ParamAccessTokenTtl"
                    },
                    "REFRESH_TOKEN_TTL": {
                        "Ref": "ParamRefreshTokenTtl"
                    }
                }
            }
        }
    },
    "Resources": {
        "MDCReminderApi": {
            "Type": "AWS::Serverless::Api",
            "Properties": {
                "EndpointConfiguration": "REGIONAL",
                "StageName": "api",
                "Cors": {
                    "AllowOrigin": "'*'",
                    "AllowMethods": "'OPTIONS,HEAD,GET,PUT,POST,PATCH,DELETE'",
                    "AllowHeaders": "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,x-id-token,x-page,x-page-size,x-sort,x-project-id,x-reminder-id'"
                }
            }
        },
        "CreateReminderFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "Runtime": "nodejs12.x",
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_auth_api.key}"
                },
                "Handler": "CreateReminder.handler",
                "Events": {
                    "ApiGatewayEvent": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/reminder/create-reminder",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCReminderApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "DeleteReminderFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "Runtime": "nodejs12.x",
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_auth_api.key}"
                },
                "Handler": "DeleteReminder.handler",
                "Events": {
                    "ApiGatewayEvent": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/reminder/remove-reminder",
                            "Method": "delete",
                            "RestApiId": {
                                "Ref": "MDCReminderApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "GetRemindersFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "Runtime": "nodejs12.x",
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_auth_api.key}"
                },
                "Handler": "GetReminders.handler",
                "Events": {
                    "ApiGatewayEvent": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/reminder/get-reminders",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCReminderApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "UpdateReminderFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "Runtime": "nodejs12.x",
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_auth_api.key}"
                },
                "Handler": "UpdateReminder.handler",
                "Events": {
                    "ApiGatewayEvent": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/reminder/update-reminder",
                            "Method": "put",
                            "RestApiId": {
                                "Ref": "MDCReminderApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        }
    },
    "Outputs": {
        "MDCReminderApi": {
            "Description": "Reminder API endpoint URL",
            "Value": { "Fn::Sub": "https://$${MDCReminderApi}.execute-api.us-eat-1.amazonaws.com/api/" }

        }
    }
}
STACK
}