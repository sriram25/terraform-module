locals  {
  environment_name_slug = join("", regexall("[[:alnum:]]+", var.environment_name))
  rds_db_schema = join("", regexall("[[:alnum:]]+", var.environment_name))
  bool_execute_migration = local.base_code_path != "" ? true : false
  bool_deploy_apis = local.base_code_path != "" ? true : false
  bool_deploy_frontend = local.base_code_path != "" ? true : false
  base_code_path = path.root
}