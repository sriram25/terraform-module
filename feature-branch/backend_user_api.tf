data "archive_file" "source_user_api" {
  type        = "zip"
  source_dir  = "${local.base_code_path}/../services/user/src/"
  output_path = "/tmp/user-${local.environment_name_slug}.zip"
  depends_on = [null_resource.services_npm_recursive_build ]
}
resource "aws_s3_bucket_object" "file_upload_user_api" {
  bucket = "frontend-dev-bucket-for-sam"
  key    = "user-${local.environment_name_slug}.zip"
  source = data.archive_file.source_user_api.output_path
}
resource "aws_cloudformation_stack" "deploy_user_api" {
  name = "${local.environment_name_slug}-user"
  capabilities = ["CAPABILITY_IAM", "CAPABILITY_AUTO_EXPAND"]
  on_failure = "DELETE"
  depends_on = [null_resource.services_npm_recursive_build ]
  parameters = {
        AppEnvironment = local.environment_name_slug
        ParamAWSID = var.aws_access_key_id
        ParamAWSKey = var.aws_secret_access_key
        ParamAWSRegion = var.aws_default_region
        ParamApiVersion = var.api_version_number
        ParamCognitoAppId = aws_cognito_user_pool_client.this.id
        ParamCognitoPool = aws_cognito_user_pool.this.id
        ParamDBHost = var.rds_db_host
        ParamDBPassword = var.rds_db_password
        ParamDBUsername = var.rds_db_username
        ParamEmailRedirectUrl = "${aws_s3_bucket.this_public.website_endpoint}/sign-up"
        ParamForgotPwdRedirect = "${aws_s3_bucket.this_public.website_endpoint}/reset-password"
        ParamS3PrivateBucket = aws_s3_bucket.this_private.id
        ParamS3PublicBucket = aws_s3_bucket.this_assets.id
        ParamVPCPrivateSubnet1 = var.vpc_private_subnet1
        ParamVPCPrivateSubnet2 = var.vpc_private_subnet2
        ParamVPCSG = var.vpc_sg
        ParamAWSRecipientEmail = var.recipient_email
        ParamAWSSenderEmail = var.sender_email
  }
  template_body = <<STACK
{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Transform": "AWS::Serverless-2016-10-31",
    "Description": "sx-mdc-service-user",
    "Globals": {
        "Function": {
            "Timeout": 10,
            "Environment": {
                "Variables": {
                    "API_VERSION": {
                        "Ref": "ParamApiVersion"
                    },
                    "COMPANY_TABLE_NAME": "company-registry-${local.environment_name_slug}",
                    "AWS_S3_ACCESS_KEY_ID": {
                        "Ref": "ParamAWSID"
                    },
                    "AWS_S3_SECRET_ACCESS_KEY": {
                        "Ref": "ParamAWSKey"
                    },
                    "AWS_S3_REGION": {
                        "Ref": "ParamAWSRegion"
                    },
                    "AWS_S3_PRIVATE_BUCKET": {
                        "Ref": "ParamS3PrivateBucket"
                    },
                    "AWS_S3_PUBLIC_BUCKET": {
                        "Ref": "ParamS3PublicBucket"
                    },
                    "POSTGRES_HOST": {
                        "Ref": "ParamDBHost"
                    },
                    "POSTGRES_USER": {
                        "Ref": "ParamDBUsername"
                    },
                    "POSTGRES_PWD": {
                        "Ref": "ParamDBPassword"
                    },
                    "PREFIX_COMMON_LIB": "/opt",
                    "MIGRATIONS_DIR": "/opt/rdb-migration/migrations",
                    "COMPANY_REGISTRY_TABLE_NAME": "company-registry-${local.environment_name_slug}",
                    "ID_TOKEN_TTL": {
                        "Ref": "ParamIdTokenTtl"
                    },
                    "ACCESS_TOKEN_TTL": {
                        "Ref": "ParamAccessTokenTtl"
                    },
                    "REFRESH_TOKEN_TTL": {
                        "Ref": "ParamRefreshTokenTtl"
                    }
                }
            }
        }
    },
    "Parameters": {
        "ParamApiVersion": {
            "Type": "String",
            "Default": "0.0.0"
        },
        "AppEnvironment": {
            "Type": "String"
        },
        "ParamDBHost": {
            "Type": "String"
        },
        "ParamDBUsername": {
            "Type": "String"
        },
        "ParamDBPassword": {
            "Type": "String"
        },
        "ParamCognitoPool": {
            "Type": "String"
        },
        "ParamCognitoAppId": {
            "Type": "String"
        },
        "ParamEmailRedirectUrl": {
            "Type": "String"
        },
        "ParamForgotPwdRedirect": {
            "Type": "String"
        },
        "ParamS3PublicBucket": {
            "Type": "String"
        },
        "ParamS3PrivateBucket": {
            "Type": "String"
        },
        "ParamVPCPrivateSubnet1": {
            "Type": "String"
        },
        "ParamVPCPrivateSubnet2": {
            "Type": "String"
        },
        "ParamVPCSG": {
            "Type": "String"
        },
        "ParamAWSRegion": {
            "Type": "String"
        },
        "ParamAWSID": {
            "Type": "String"
        },
        "ParamAWSKey": {
            "Type": "String"
        },
        "ParamAWSSenderEmail": {
            "Type": "String"
        },
        "ParamAWSRecipientEmail": {
            "Type": "String"
        },
        "ParamIdTokenTtl": {
            "Type": "String",
            "Default": 1
        },
        "ParamAccessTokenTtl": {
            "Type": "String",
            "Default": 1
        },
        "ParamRefreshTokenTtl": {
            "Type": "String",
            "Default": 30
        }
    },
    "Resources": {
        "MDCUserApi": {
            "Type": "AWS::Serverless::Api",
            "Properties": {
                "EndpointConfiguration": "REGIONAL",
                "StageName": "api",
                "Cors": {
                    "AllowOrigin": "'*'",
                    "AllowMethods": "'OPTIONS,HEAD,GET,PUT,POST,PATCH,DELETE'",
                    "AllowHeaders": "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,x-id-token,x-file-path,x-user-email,x-user-details-id'"
                }
            }
        },
        "GetUserInfoFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_user_api.key}"
                },
                "Handler": "GetUserInfo.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetUserInfo": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/user/get-user-info",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCUserApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "CreateUserProfileFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_user_api.key}"
                },
                "Handler": "CreateUserProfile.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "CreateUserProfile": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/user/create-user-profile",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCUserApi"
                            }
                        }
                    }
                },
                "Environment": {
                    "Variables": {
                        "AWS_COGNITO_USER_POOL_ID": {
                            "Ref": "ParamCognitoPool"
                        }
                    }
                },
                "Policies": [
                    "AmazonS3FullAccess",
                    "AmazonCognitoPowerUser",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "CreateUserMilestoneFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_user_api.key}"
                },
                "Handler": "CreateUserMilestone.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "Api": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/user/create-milestone",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCUserApi"
                            }
                        }
                    }
                },
                "Environment": {
                    "Variables": {
                        "AWS_COGNITO_USER_POOL_ID": {
                            "Ref": "ParamCognitoPool"
                        }
                    }
                },
                "Policies": [
                    {
                        "DynamoDBCrudPolicy": {
                            "TableName": "company-registry-${local.environment_name_slug}"
                        }
                    },
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "GetUserMilestonesFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_user_api.key}"
                },
                "Handler": "GetUserMilestones.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "Api": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/user/get-milestones",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCUserApi"
                            }
                        }
                    }
                },
                "Environment": {
                    "Variables": {
                        "AWS_COGNITO_USER_POOL_ID": {
                            "Ref": "ParamCognitoPool"
                        }
                    }
                },
                "Policies": [
                    {
                        "DynamoDBCrudPolicy": {
                            "TableName": "company-registry-${local.environment_name_slug}"
                        }
                    },
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "GetUsersFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_user_api.key}"
                },
                "Handler": "GetUsers.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetInvitedUsers": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/user/get-users",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCUserApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "UploadProfileImageFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_user_api.key}"
                },
                "Handler": "UploadProfileImage.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "UploadProfileImage": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/user/upload-profile-image",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCUserApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonS3FullAccess"
                ]
            }
        },
        "DownloadProfileImageFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_user_api.key}"
                },
                "Handler": "DownloadProfileImage.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "DownloadProfileImage": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/user/download-profile-image",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCUserApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonS3ReadOnlyAccess"
                ]
            }
        },
        "SendHelpTicketFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
              "CodeUri": {
                "Bucket": "frontend-dev-bucket-for-sam",
                "Key": "${aws_s3_bucket_object.file_upload_user_api.key}"
              },
              "Handler": "SendHelpTicket.handler",
              "Layers": [
                "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
              ],
              "Runtime": "nodejs12.x",
              "Events": {
                "SendHelpTicket": {
                  "Type": "Api",
                  "Properties": {
                    "Path": "/user/send-help-ticket",
                    "Method": "post",
                    "RestApiId": {
                      "Ref": "MDCUserApi"
                    }
                  }
                }
              }
            }
        },
        "SendErrorTicketFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_user_api.key}"
                },
                "Handler": "SendErrorTicket.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "SendErrorTicket": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/user/send-error-ticket",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCUserApi"
                            }
                        }
                    }
                },
                "Environment": {
                  "Variables": {
                    "AWS_COGNITO_USER_POOL_ID": {
                      "Ref": "ParamCognitoPool"
                    },
                    "AWS_SES_S3_NOTIFICATION_SENDER_EMAIL": {
                      "Ref": "ParamAWSSenderEmail"
                    },
                    "AWS_SES_S3_NOTIFICATION_RECIPIENT_EMAIL": {
                      "Ref": "ParamAWSRecipientEmail"
                    }
                  }
                },
                "Policies": [
                    "AmazonSESFullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "UpdateUserProfileFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_user_api.key}"
                },
                "Handler": "UpdateUserProfile.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "CreateUserProfile": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/user/update-user-profile",
                            "Method": "put",
                            "RestApiId": {
                                "Ref": "MDCUserApi"
                            }
                        }
                    }
                },
                "Environment": {
                    "Variables": {
                      "AWS_COGNITO_USER_POOL_ID": {
                        "Ref": "ParamCognitoPool"
                      }
                    }
                },
                "Policies": [
                    "AmazonCognitoPowerUser",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        }
    },
    "Outputs": {
        "MDCUserApi": {
            "Description": "User API endpoint URL",
            "Value": { "Fn::Sub": "https://$${MDCUserApi}.execute-api.us-eat-1.amazonaws.com/api/" }

        }
    }
}
STACK
}