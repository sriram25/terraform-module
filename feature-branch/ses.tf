locals {
  raw_VendorMaterialDelivered = jsondecode(file("${local.base_code_path}/../services/common/src/templates/VendorMaterialDelivered.json"))
  raw_TaskOwnerNotification = jsondecode(file("${local.base_code_path}/../services/common/src/templates/TaskOwnerNotification.json"))
  raw_SubTaskOwnerNotification = jsondecode(file("${local.base_code_path}/../services/common/src/templates/SubTaskOwnerNotification.json"))
  raw_ProjectOwnerNotification = jsondecode(file("${local.base_code_path}/../services/common/src/templates/ProjectOwnerNotification.json"))
  raw_PrefabMaterialDeliveredNotification = jsondecode(file("${local.base_code_path}/../services/common/src/templates/PrefabMaterialDelivered.json"))
  raw_IssueOwnerNotification = jsondecode(file("${local.base_code_path}/../services/common/src/templates/IssueAssignmentNotification.json"))
 // raw_ReminderNotification = jsondecode(file("${local.base_code_path}/../services/common/src/templates/ReminderNotification.json"))
  raw_TradeScheduleUploaded = jsondecode(file("${local.base_code_path}/../services/common/src/templates/TradeScheduleUploaded.json"))
  raw_ErrorTicket = jsondecode(file("${local.base_code_path}/../services/common/src/templates/ErrorTicket.json"))
  raw_HelpTicket = jsondecode(file("${local.base_code_path}/../services/common/src/templates/HelpTicket.json"))

}

resource "aws_ses_template" "vendorMaterialDeliveredNotification" {
  name    = "${local.raw_VendorMaterialDelivered.Template.TemplateName}-${local.environment_name_slug}"
  subject = local.raw_VendorMaterialDelivered.Template.SubjectPart
  html    = local.raw_VendorMaterialDelivered.Template.HtmlPart
  text    = local.raw_VendorMaterialDelivered.Template.TextPart
}

resource "aws_ses_template" "TaskOwnerNotification" {
  name    = "${local.raw_TaskOwnerNotification.Template.TemplateName}-${local.environment_name_slug}"
  subject = local.raw_TaskOwnerNotification.Template.SubjectPart
  html    = local.raw_TaskOwnerNotification.Template.HtmlPart
  text    = local.raw_TaskOwnerNotification.Template.TextPart
}
resource "aws_ses_template" "SubTaskOwnerNotification" {
  name    = "${local.raw_SubTaskOwnerNotification.Template.TemplateName}-${local.environment_name_slug}"
  subject = local.raw_SubTaskOwnerNotification.Template.SubjectPart
  html    = local.raw_SubTaskOwnerNotification.Template.HtmlPart
  text    = local.raw_SubTaskOwnerNotification.Template.TextPart
}
resource "aws_ses_template" "ProjectOwnerNotification" {
  name    = "${local.raw_ProjectOwnerNotification.Template.TemplateName}-${local.environment_name_slug}"
  subject = local.raw_ProjectOwnerNotification.Template.SubjectPart
  html    = local.raw_ProjectOwnerNotification.Template.HtmlPart
  text    = local.raw_ProjectOwnerNotification.Template.TextPart
}
resource "aws_ses_template" "PrefabMaterialDeliveredNotification" {
  name    = "${local.raw_PrefabMaterialDeliveredNotification.Template.TemplateName}-${local.environment_name_slug}"
  subject = local.raw_PrefabMaterialDeliveredNotification.Template.SubjectPart
  html    = local.raw_PrefabMaterialDeliveredNotification.Template.HtmlPart
  text    = local.raw_PrefabMaterialDeliveredNotification.Template.TextPart
}

resource "aws_ses_template" "IssueOwnerNotification" {
  name    = "${local.raw_IssueOwnerNotification.Template.TemplateName}-${local.environment_name_slug}"
  subject = local.raw_IssueOwnerNotification.Template.SubjectPart
  html    = local.raw_IssueOwnerNotification.Template.HtmlPart
  text    = local.raw_IssueOwnerNotification.Template.TextPart
}
/*
resource "aws_ses_template" "ReminderNotification" {
  name    = local.raw_ReminderNotification.Template.TemplateName}-${local.environment_name_slug}"
  subject = local.raw_ReminderNotification.Template.SubjectPart
  html    = local.raw_ReminderNotification.Template.HtmlPart
  text    = local.raw_ReminderNotification.Template.TextPart
}
*/
resource "aws_ses_template" "TradeScheduleUploaded" {
  name    = "${local.raw_TradeScheduleUploaded.Template.TemplateName}-${local.environment_name_slug}"
  subject = local.raw_TradeScheduleUploaded.Template.SubjectPart
  html    = local.raw_TradeScheduleUploaded.Template.HtmlPart
  text    = local.raw_TradeScheduleUploaded.Template.TextPart
}
resource "aws_ses_template" "ErrorTicket" {
  name    = "${local.raw_ErrorTicket.Template.TemplateName}-${local.environment_name_slug}"
  subject = local.raw_ErrorTicket.Template.SubjectPart
  html    = local.raw_ErrorTicket.Template.HtmlPart
  text    = local.raw_ErrorTicket.Template.TextPart
}
resource "aws_ses_template" "HelpTicket" {
  name    = "${local.raw_HelpTicket.Template.TemplateName}-${local.environment_name_slug}"
  subject = local.raw_HelpTicket.Template.SubjectPart
  html    = local.raw_HelpTicket.Template.HtmlPart
  text    = local.raw_HelpTicket.Template.TextPart
}
