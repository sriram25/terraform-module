resource "aws_s3_bucket" "this_public" {
  bucket = local.environment_name_slug
  acl = "public-read"
  website {
    index_document = "index.html"
    error_document = "index.html"
  }
}
resource "aws_s3_bucket" "this_private" {
  bucket = "${local.environment_name_slug}-private"
}
resource "aws_s3_bucket" "this_assets" {
  bucket = "${local.environment_name_slug}-assets"
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT", "POST", "HEAD", "GET" ]
    allowed_origins = ["*"]
    expose_headers  = [""]
    max_age_seconds = 3000
  }
}
resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.this_assets.id
  policy = "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Principal\":{\"AWS\":\"${var.S3_ADMIN_ROLE_ARN}\"},\"Action\":\"s3:*\",\"Resource\":\"arn:aws:s3:::${local.environment_name_slug}-assets/*\"}]}"
}