data "archive_file" "source_issue_api" {
  type        = "zip"
  source_dir  = "${local.base_code_path}/../services/issue/src/"
  output_path = "/tmp/issue-${local.environment_name_slug}.zip"
  depends_on = [null_resource.services_npm_recursive_build ]
}
resource "aws_s3_bucket_object" "file_upload_issue_api" {
  bucket = "frontend-dev-bucket-for-sam"
  key    = "issue-${local.environment_name_slug}.zip"
  source = data.archive_file.source_issue_api.output_path
}
resource "aws_cloudformation_stack" "deploy_issue_api" {
  name = "${local.environment_name_slug}-issue"
  capabilities = [
    "CAPABILITY_IAM",
    "CAPABILITY_AUTO_EXPAND"]
  on_failure = "DELETE"
  depends_on = [
    null_resource.services_npm_recursive_build]
  parameters = {
    AppEnvironment = local.environment_name_slug
    ParamAWSID = var.aws_access_key_id
    ParamAWSKey = var.aws_secret_access_key
    ParamAWSRecipientEmail = var.recipient_email
    ParamAWSRegion = var.aws_default_region
    ParamAWSSenderEmail = var.sender_email
    ParamApiVersion = var.api_version_number
    ParamCognitoAppId = aws_cognito_user_pool_client.this.id
    ParamCognitoPool = aws_cognito_user_pool.this.id
    ParamDBHost = var.rds_db_host
    ParamDBPassword = var.rds_db_password
    ParamDBUsername = var.rds_db_username
    ParamEmailRedirectUrl = "${aws_s3_bucket.this_public.website_endpoint}/sign-up"
    ParamForgotPwdRedirect = "${aws_s3_bucket.this_public.website_endpoint}/reset-password"
    ParamAwsNotificationTableName = aws_dynamodb_table.this_default_dynamodb_table.name
    ParamS3PrivateBucket = aws_s3_bucket.this_private.id
    ParamS3PublicBucket = aws_s3_bucket.this_assets.id
    ParamVPCPrivateSubnet1 = var.vpc_private_subnet1
    ParamVPCPrivateSubnet2 = var.vpc_private_subnet2
    ParamVPCSG = var.vpc_sg
  }
  template_body = <<STACK
{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Transform": "AWS::Serverless-2016-10-31",
    "Description": "sx-mdc-service-issue",
    "Globals": {
        "Function": {
            "Timeout": 10,
            "Environment": {
                "Variables": {
                    "API_VERSION": {
                        "Ref": "ParamApiVersion"
                    },
                    "AWS_S3_ACCESS_KEY_ID": {
                        "Ref": "ParamAWSID"
                    },
                    "AWS_S3_SECRET_ACCESS_KEY": {
                        "Ref": "ParamAWSKey"
                    },
                    "AWS_S3_REGION": {
                        "Ref": "ParamAWSRegion"
                    },
                    "AWS_S3_PRIVATE_BUCKET": {
                        "Ref": "ParamS3PrivateBucket"
                    },
                    "AWS_S3_PUBLIC_BUCKET": {
                        "Ref": "ParamS3PublicBucket"
                    },
                    "POSTGRES_HOST": {
                        "Ref": "ParamDBHost"
                    },
                    "POSTGRES_USER": {
                        "Ref": "ParamDBUsername"
                    },
                    "POSTGRES_PWD": {
                        "Ref": "ParamDBPassword"
                    },
                    "PREFIX_COMMON_LIB": "/opt",
                    "MIGRATIONS_DIR": "/opt/rdb-migration/migrations",
                    "COMPANY_REGISTRY_TABLE_NAME": "${aws_dynamodb_table.this_company_registry_table.name}",
                    "ID_TOKEN_TTL": {
                        "Ref": "ParamIdTokenTtl"
                    },
                    "ACCESS_TOKEN_TTL": {
                        "Ref": "ParamAccessTokenTtl"
                    },
                    "REFRESH_TOKEN_TTL": {
                        "Ref": "ParamRefreshTokenTtl"
                    }
                }
            }
        }
    },
    "Parameters": {
        "ParamApiVersion": {
            "Type": "String",
            "Default": "0.0.0"
        },
        "AppEnvironment": {
            "Type": "String"
        },
        "ParamDBHost": {
            "Type": "String"
        },
        "ParamDBUsername": {
            "Type": "String"
        },
        "ParamDBPassword": {
            "Type": "String"
        },
        "ParamCognitoPool": {
            "Type": "String"
        },
        "ParamCognitoAppId": {
            "Type": "String"
        },
        "ParamEmailRedirectUrl": {
            "Type": "String"
        },
        "ParamForgotPwdRedirect": {
            "Type": "String"
        },
        "ParamS3PublicBucket": {
            "Type": "String"
        },
        "ParamS3PrivateBucket": {
            "Type": "String"
        },
        "ParamVPCPrivateSubnet1": {
            "Type": "String"
        },
        "ParamVPCPrivateSubnet2": {
            "Type": "String"
        },
        "ParamVPCSG": {
            "Type": "String"
        },
        "ParamAWSRegion": {
            "Type": "String"
        },
        "ParamAWSID": {
            "Type": "String"
        },
        "ParamAWSKey": {
            "Type": "String"
        },
        "ParamAWSSenderEmail": {
            "Type": "String"
        },
        "ParamAWSRecipientEmail": {
            "Type": "String"
        },
        "ParamAwsNotificationTableName": {
            "Type": "String"
        },
        "ParamIdTokenTtl": {
            "Type": "String",
            "Default": 1
        },
        "ParamAccessTokenTtl": {
            "Type": "String",
            "Default": 1
        },
        "ParamRefreshTokenTtl": {
            "Type": "String",
            "Default": 30
        }
    },
    "Resources": {
        "MDCIssueApi": {
            "Type": "AWS::Serverless::Api",
            "Properties": {
                "EndpointConfiguration": "REGIONAL",
                "StageName": "api",
                "Cors": {
                    "AllowOrigin": "'*'",
                    "AllowMethods": "'OPTIONS,HEAD,GET,PUT,POST,PATCH,DELETE'",
                    "AllowHeaders": "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,x-id-token,x-file-path,x-datetime,x-issue-id,x-resource-id,x-resource-type,x-attachment-id,x-issue-comment-id,x-page,x-page-size,x-sort,x-task-id,x-start-date,x-end-date,x-issue-type,x-issue-status'"
                }
            }
        },
        "CreateIssueFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_issue_api.key}"
                },
                "Handler": "CreateIssue.handler",
                "Layers": [
                    "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "CreateIssue": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/issue/create-issue",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCIssueApi"
                            }
                        }
                    }
                },
                "Environment": {
                    "Variables": {
                        "AWS_DYNAMO_NOTIFICATION_TABLE": {
                            "Ref": "ParamAwsNotificationTableName"
                        }
                    }
                },
                "Policies": [
                    "AmazonS3FullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonDynamoDBFullAccess"
                ]
            }
        },
        "CreateIssueAttachmentsFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_issue_api.key}"
                },
                "Handler": "CreateIssueAttachments.handler",
                "Layers": [
                      "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "CreateIssue": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/issue/create-issue-attachment",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCIssueApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonS3FullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "CreateIssueCommentFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_issue_api.key}"
                },
                "Handler": "CreateIssueComment.handler",
                "Layers": [
                     "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "CreateIssue": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/issue/create-issue-comment",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCIssueApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonS3FullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "DeleteIssueFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_issue_api.key}"
                },
                "Handler": "DeleteIssue.handler",
                "Layers": [
                   "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "DeleteIssue": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/issue/remove-issue",
                            "Method": "delete",
                            "RestApiId": {
                                "Ref": "MDCIssueApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonS3FullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "DeleteIssueAttachmentFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_issue_api.key}"
                },
                "Handler": "DeleteIssueAttachment.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "CreateIssue": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/issue/remove-issue-attachment",
                            "Method": "delete",
                            "RestApiId": {
                                "Ref": "MDCIssueApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonS3FullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "DeleteIssueCommentFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_issue_api.key}"
                },
                "Handler": "DeleteIssueComment.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "CreateIssue": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/issue/remove-issue-comment",
                            "Method": "delete",
                            "RestApiId": {
                                "Ref": "MDCIssueApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonS3FullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "GetIssueAttachmentsFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_issue_api.key}"
                },
                "Handler": "GetIssueAttachments.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "CreateIssue": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/issue/get-issue-attachments",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCIssueApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonS3FullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "GetIssueCommentsFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_issue_api.key}"
                },
                "Handler": "GetIssueComments.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "CreateIssue": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/issue/get-issue-comments",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCIssueApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonS3FullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "GetIssues": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_issue_api.key}"
                },
                "Handler": "GetIssues.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "CreateIssue": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/issue/get-issues",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCIssueApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonS3FullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "GetIssue": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_issue_api.key}"
                },
                "Handler": "GetIssue.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "Api": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/issue/get-issue",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCIssueApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "UpdateIssueFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_issue_api.key}"
                },
                "Handler": "UpdateIssue.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "CreateIssue": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/issue/update-issue",
                            "Method": "put",
                            "RestApiId": {
                                "Ref": "MDCIssueApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonS3FullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "UpdateIssueCommentFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_issue_api.key}"
                },
                "Handler": "UpdateIssueComment.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "CreateIssue": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/issue/update-issue-comment",
                            "Method": "put",
                            "RestApiId": {
                                "Ref": "MDCIssueApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonS3FullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ],
                "VpcConfig": {
                    "SecurityGroupIds": [
                        {
                            "Ref": "ParamVPCSG"
                        }
                    ],
                    "SubnetIds": [
                        {
                            "Ref": "ParamVPCPrivateSubnet1"
                        },
                        {
                            "Ref": "ParamVPCPrivateSubnet2"
                        }
                    ]
                }
            }
        },
        "UploadManager": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_issue_api.key}"
                },
                "Handler": "UploadManager.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "CreateIssue": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/upload",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCIssueApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonS3FullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        }
    },
    "Outputs": {
        "MDCIssueApi": {
            "Description": "Issue API endpoint URL",
            "Value": { "Fn::Sub": "https://$${MDCIssueApi}.execute-api.us-eat-1.amazonaws.com/api/" }

        }
    }
}
STACK
}