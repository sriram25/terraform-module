## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_archive"></a> [archive](#provider\_archive) | n/a |
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 3.0 |
| <a name="provider_null"></a> [null](#provider\_null) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_cloudformation_stack.db-migrations](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudformation_stack) | resource |
| [aws_cloudformation_stack.deploy_auth_api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudformation_stack) | resource |
| [aws_cloudformation_stack.deploy_bootstrap_api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudformation_stack) | resource |
| [aws_cloudformation_stack.deploy_common_layer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudformation_stack) | resource |
| [aws_cloudformation_stack.deploy_issue_api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudformation_stack) | resource |
| [aws_cloudformation_stack.deploy_listview_api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudformation_stack) | resource |
| [aws_cloudformation_stack.deploy_notifications_api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudformation_stack) | resource |
| [aws_cloudformation_stack.deploy_project_api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudformation_stack) | resource |
| [aws_cloudformation_stack.deploy_reminder_api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudformation_stack) | resource |
| [aws_cloudformation_stack.deploy_user_api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudformation_stack) | resource |
| [aws_cognito_user_pool.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_user_pool) | resource |
| [aws_cognito_user_pool_client.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cognito_user_pool_client) | resource |
| [aws_dynamodb_table.this_company_registry_table](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table) | resource |
| [aws_dynamodb_table.this_default_dynamodb_table](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table) | resource |
| [aws_iam_role.iam_lambda_notification_delivery](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_s3_bucket.this_assets](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket.this_private](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket.this_public](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_object.file_upload](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_object) | resource |
| [aws_s3_bucket_object.file_upload_auth_api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_object) | resource |
| [aws_s3_bucket_object.file_upload_bootstrap_api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_object) | resource |
| [aws_s3_bucket_object.file_upload_common_layer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_object) | resource |
| [aws_s3_bucket_object.file_upload_issue_api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_object) | resource |
| [aws_s3_bucket_object.file_upload_listview_api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_object) | resource |
| [aws_s3_bucket_object.file_upload_notifications_api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_object) | resource |
| [aws_s3_bucket_object.file_upload_project_api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_object) | resource |
| [aws_s3_bucket_object.file_upload_reminder_api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_object) | resource |
| [aws_s3_bucket_object.file_upload_user_api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_object) | resource |
| [aws_s3_bucket_policy.bucket_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_policy) | resource |
| [aws_ses_template.ErrorTicket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ses_template) | resource |
| [aws_ses_template.HelpTicket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ses_template) | resource |
| [aws_ses_template.IssueOwnerNotification](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ses_template) | resource |
| [aws_ses_template.PrefabMaterialDeliveredNotification](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ses_template) | resource |
| [aws_ses_template.ProjectOwnerNotification](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ses_template) | resource |
| [aws_ses_template.SubTaskOwnerNotification](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ses_template) | resource |
| [aws_ses_template.TaskOwnerNotification](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ses_template) | resource |
| [aws_ses_template.TradeScheduleUploaded](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ses_template) | resource |
| [aws_ses_template.vendorMaterialDeliveredNotification](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ses_template) | resource |
| [aws_sns_topic.topic_immediate_notification](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic) | resource |
| [aws_sns_topic_policy.policy_topic_immediate_notification](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic_policy) | resource |
| [null_resource.create-schema](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.create-schema-role](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.frontend_build_deploy](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.resources_npm_recursive_build](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.services_npm_recursive_build](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [archive_file.source](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [archive_file.source_auth_api](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [archive_file.source_bootstrap_api](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [archive_file.source_common_layer](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [archive_file.source_issue_api](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [archive_file.source_listview_api](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [archive_file.source_notifications_api](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [archive_file.source_project_api](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [archive_file.source_reminder_api](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [archive_file.source_user_api](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [aws_lambda_invocation.schema_setup](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/lambda_invocation) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_PREFIX_COMMON_LIB"></a> [PREFIX\_COMMON\_LIB](#input\_PREFIX\_COMMON\_LIB) | n/a | `string` | `"'../../common/src'"` | no |
| <a name="input_S3_ADMIN_ROLE_ARN"></a> [S3\_ADMIN\_ROLE\_ARN](#input\_S3\_ADMIN\_ROLE\_ARN) | n/a | `string` | n/a | yes |
| <a name="input_api_version_number"></a> [api\_version\_number](#input\_api\_version\_number) | n/a | `string` | `"0.0.0"` | no |
| <a name="input_aws_access_key_id"></a> [aws\_access\_key\_id](#input\_aws\_access\_key\_id) | n/a | `string` | n/a | yes |
| <a name="input_aws_account_id"></a> [aws\_account\_id](#input\_aws\_account\_id) | n/a | `string` | n/a | yes |
| <a name="input_aws_default_region"></a> [aws\_default\_region](#input\_aws\_default\_region) | n/a | `string` | `"us-east-1"` | no |
| <a name="input_aws_secret_access_key"></a> [aws\_secret\_access\_key](#input\_aws\_secret\_access\_key) | n/a | `string` | n/a | yes |
| <a name="input_custom_message_lambda_arn"></a> [custom\_message\_lambda\_arn](#input\_custom\_message\_lambda\_arn) | n/a | `string` | `""` | no |
| <a name="input_database_credential_secret_arn"></a> [database\_credential\_secret\_arn](#input\_database\_credential\_secret\_arn) | n/a | `string` | n/a | yes |
| <a name="input_deploy_code"></a> [deploy\_code](#input\_deploy\_code) | n/a | `bool` | n/a | yes |
| <a name="input_environment_name"></a> [environment\_name](#input\_environment\_name) | Feature Branch Name | `string` | n/a | yes |
| <a name="input_mix_panel_token"></a> [mix\_panel\_token](#input\_mix\_panel\_token) | n/a | `string` | n/a | yes |
| <a name="input_notification_retention_days"></a> [notification\_retention\_days](#input\_notification\_retention\_days) | n/a | `string` | `"15"` | no |
| <a name="input_notifications_sender_email"></a> [notifications\_sender\_email](#input\_notifications\_sender\_email) | n/a | `string` | `"noreply@example.com"` | no |
| <a name="input_rds_db_host"></a> [rds\_db\_host](#input\_rds\_db\_host) | n/a | `string` | n/a | yes |
| <a name="input_rds_db_mdc_owner"></a> [rds\_db\_mdc\_owner](#input\_rds\_db\_mdc\_owner) | n/a | `string` | `"postgres"` | no |
| <a name="input_rds_db_name"></a> [rds\_db\_name](#input\_rds\_db\_name) | n/a | `string` | `"postgres"` | no |
| <a name="input_rds_db_password"></a> [rds\_db\_password](#input\_rds\_db\_password) | n/a | `string` | n/a | yes |
| <a name="input_rds_db_port"></a> [rds\_db\_port](#input\_rds\_db\_port) | n/a | `string` | `"5432"` | no |
| <a name="input_rds_db_username"></a> [rds\_db\_username](#input\_rds\_db\_username) | n/a | `string` | `"postgres"` | no |
| <a name="input_rds_instance_arn"></a> [rds\_instance\_arn](#input\_rds\_instance\_arn) | n/a | `string` | n/a | yes |
| <a name="input_recipient_email"></a> [recipient\_email](#input\_recipient\_email) | n/a | `string` | `"noreply@example.com"` | no |
| <a name="input_s3_bucket_for_sam"></a> [s3\_bucket\_for\_sam](#input\_s3\_bucket\_for\_sam) | n/a | `string` | `"frontend-dev-bucket-for-sam"` | no |
| <a name="input_sender_email"></a> [sender\_email](#input\_sender\_email) | n/a | `string` | `"noreply@example.com"` | no |
| <a name="input_sns_immediate_notification_topic"></a> [sns\_immediate\_notification\_topic](#input\_sns\_immediate\_notification\_topic) | n/a | `string` | `"test"` | no |
| <a name="input_vpc_private_subnet1"></a> [vpc\_private\_subnet1](#input\_vpc\_private\_subnet1) | n/a | `string` | n/a | yes |
| <a name="input_vpc_private_subnet2"></a> [vpc\_private\_subnet2](#input\_vpc\_private\_subnet2) | n/a | `string` | n/a | yes |
| <a name="input_vpc_sg"></a> [vpc\_sg](#input\_vpc\_sg) | n/a | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_aws_cognito_user_pool_client_default_app_id"></a> [aws\_cognito\_user\_pool\_client\_default\_app\_id](#output\_aws\_cognito\_user\_pool\_client\_default\_app\_id) | n/a |
| <a name="output_aws_cognito_user_pool_default_id"></a> [aws\_cognito\_user\_pool\_default\_id](#output\_aws\_cognito\_user\_pool\_default\_id) | n/a |
| <a name="output_dynamodb_default_table_stream_arn"></a> [dynamodb\_default\_table\_stream\_arn](#output\_dynamodb\_default\_table\_stream\_arn) | n/a |
| <a name="output_dynamodb_registry_table_name"></a> [dynamodb\_registry\_table\_name](#output\_dynamodb\_registry\_table\_name) | n/a |
| <a name="output_frontend_url"></a> [frontend\_url](#output\_frontend\_url) | n/a |
| <a name="output_result_entry"></a> [result\_entry](#output\_result\_entry) | n/a |
