data "archive_file" "source" {
  type        = "zip"
  source_dir  = "${local.base_code_path}/../Resources/rdb-migration/"
  output_path = "/tmp/rdb-migration-${local.environment_name_slug}.zip"
  depends_on = [null_resource.resources_npm_recursive_build ]
}
# upload zip to s3 and then update lambda function from s3
resource "aws_s3_bucket_object" "file_upload" {
  bucket = "frontend-dev-bucket-for-sam"
  key    = "rdb-migration-${local.environment_name_slug}.zip"
  source = data.archive_file.source.output_path
}

resource "aws_cloudformation_stack" "db-migrations" {
  name = "${local.environment_name_slug}-db-migration-tf"
  capabilities = ["CAPABILITY_IAM", "CAPABILITY_AUTO_EXPAND"]
  on_failure = "DO_NOTHING"
  depends_on = [null_resource.resources_npm_recursive_build ]
  parameters = {
    ParamDBHost = var.rds_db_host
    ParamDBPort = var.rds_db_port
    ParamDBName = var.rds_db_name
    ParamDBSchema = local.environment_name_slug
    ParamDBUsername = var.rds_db_username
    ParamDBPassword = var.rds_db_password
    ParamDBMDCOwner = var.rds_db_mdc_owner
    ParamVPCSG = var.vpc_sg
    ParamVPCPrivateSubnet1 = var.vpc_private_subnet1
    ParamVPCPrivateSubnet2 = var.vpc_private_subnet2
    ParamDbMigrationCodePath = "${local.base_code_path}/../Resources/rdb-migration/"
  }
  template_body = <<STACK
{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Transform": "AWS::Serverless-2016-10-31",
    "Description": "db-migrate lambda for ${local.environment_name_slug}",
    "Globals": {
        "Function": {
            "Timeout": 30,
            "VpcConfig": {
                "SecurityGroupIds": [
                    {
                        "Ref": "ParamVPCSG"
                    }
                ],
                "SubnetIds": [
                    {
                        "Ref": "ParamVPCPrivateSubnet1"
                    },
                    {
                        "Ref": "ParamVPCPrivateSubnet2"
                    }
                ]
            },
            "Environment": {
                "Variables": {
                    "PG_HOST": {
                        "Ref": "ParamDBHost"
                    },
                    "PG_PORT": {
                        "Ref": "ParamDBPort"
                    },
                    "PG_DATABASE": {
                        "Ref": "ParamDBName"
                    },
                    "PG_SCHEMA": {
                        "Ref": "ParamDBSchema"
                    },
                    "PG_USERNAME": {
                        "Ref": "ParamDBUsername"
                    },
                    "PG_PASSWORD": {
                        "Ref": "ParamDBPassword"
                    },
                    "MDC_OWNER": {
                        "Ref": "ParamDBMDCOwner"
                    }
                }
            },
            "Runtime": "nodejs12.x"
        }
    },
    "Parameters": {
        "ParamDBHost": {
            "Type": "String"
        },
        "ParamDBPort": {
            "Type": "String"
        },
        "ParamDBName": {
            "Type": "String"
        },
        "ParamDBSchema": {
            "Type": "String"
        },
        "ParamDBUsername": {
            "Type": "String"
        },
        "ParamDBPassword": {
            "Type": "String"
        },
        "ParamDBMDCOwner": {
            "Type": "String"
        },
        "ParamVPCSG": {
            "Type": "String"
        },
        "ParamVPCPrivateSubnet1": {
            "Type": "String"
        },
        "ParamVPCPrivateSubnet2": {
            "Type": "String"
        },
        "ParamDbMigrationCodePath": {
            "Type": "String"
        }
    },
    "Resources": {
        "MigrateUpFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload.key}"
                },
                "Handler": "migrationUpLambda.handler"
            }
        },
        "MigrateResetFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload.key}"
                },
                "Handler": "migrationResetLambda.handler"
            }
        }
    },
    "Outputs": {
        "MigrateUpFunctionname": {
            "Description": "Migrate Lambda function",
            "Value": { "Ref": "MigrateUpFunction" }
        }
    }
}
STACK
}
data "aws_lambda_invocation" "schema_setup" {
  function_name =  aws_cloudformation_stack.db-migrations.outputs.MigrateUpFunctionname

  input = <<JSON
{
  "key1": "value1",
  "key2": "value2"
}
JSON
}

output "result_entry" {
  value = jsondecode(data.aws_lambda_invocation.schema_setup.result)["key1"]
}