resource "null_resource" "create-schema-role" {
  provisioner "local-exec" {
    command = "aws rds-data execute-statement --secret-arn ${var.database_credential_secret_arn} --resource-arn ${var.rds_instance_arn} --database postgres --sql \" DO \\$\\$ BEGIN CREATE ROLE ${local.environment_name_slug} ; EXCEPTION WHEN DUPLICATE_OBJECT THEN RAISE NOTICE 'not creating user. Already exists!' ; END \\$\\$ ; \" "
  }
}
resource "null_resource" "create-schema" {
  provisioner "local-exec" {
    command = "aws rds-data execute-statement --secret-arn ${var.database_credential_secret_arn} --resource-arn ${var.rds_instance_arn} --database ${var.rds_db_name} --sql \"GRANT ${local.environment_name_slug} TO postgres; CREATE SCHEMA IF NOT EXISTS AUTHORIZATION ${local.environment_name_slug} \" ;"
  }
  depends_on = [null_resource.create-schema-role]
}

# @todo add feature to have unique database username and password
# condition to execute only during resource creation