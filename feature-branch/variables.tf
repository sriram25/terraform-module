variable "environment_name" {
  description = "Feature Branch Name"
  type        = string
  //  default     = "example_feature_branch"
}
variable "database_credential_secret_arn" {
  type = string
  //  default = "arn:aws:secretsmanager:us-east-1:540116054793:secret:rds-db-credentials/cluster-6QZNR2M5DMOYXVTOQ2H2HL6I6U/postgres-6TN3Fx"
}
variable "rds_instance_arn" {
  type = string
  //  default = "arn:aws:rds:us-east-1:540116054793:cluster:database-1"
}
variable "rds_db_name" {
  type = string
  default = "postgres"
}
variable "rds_db_username" {
  type = string
  default = "postgres"
}
variable "rds_db_password" {
  type = string
}
variable "rds_db_mdc_owner" {
  type = string
  default = "postgres"
}
variable "rds_db_host" {
  type = string
}
variable "rds_db_port" {
  type = string
  default = "5432"
}
variable "PREFIX_COMMON_LIB" {
  description = ""
  type        = string
  default     = "'../../common/src'"
}
variable "S3_ADMIN_ROLE_ARN" {
  type = string
  //  default = "arn:aws:iam::540116054793:user/srirams"
}
variable "custom_message_lambda_arn" {
  type = string
  default = ""
}
variable "vpc_sg"  {
  type = string
}
variable "vpc_private_subnet1" {
  type = string
}
variable "vpc_private_subnet2" {
  type = string

}
variable "s3_bucket_for_sam" {
  type = string
  default = "frontend-dev-bucket-for-sam"
}
variable "api_version_number" {
  type = string
  default = "0.0.0"
}
variable "notifications_sender_email" {
  type = string
  default = "noreply@example.com"
}
variable "sender_email" {
  type = string
  default = "noreply@example.com"
}
variable "sns_immediate_notification_topic" {
  type = string
  default = "test"
}
variable "recipient_email" {
  type = string
  default = "noreply@example.com"
}
variable "notification_retention_days" {
  type = string
  default = "15"
}
variable "aws_access_key_id" {
  type = string
}
variable "aws_secret_access_key" {
  type = string
}
variable "aws_default_region" {
  type = string
  default = "us-east-1"
}
variable "deploy_code" {
  type = bool
}
variable "aws_account_id" {
  type = string
}
variable "mix_panel_token" {
  type = string
}