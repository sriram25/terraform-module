data "archive_file" "source_auth_api" {
  type        = "zip"
  source_dir  = "${local.base_code_path}/../services/auth/src/"
  output_path = "/tmp/auth-${local.environment_name_slug}.zip"
  depends_on = [null_resource.services_npm_recursive_build ]
}
resource "aws_s3_bucket_object" "file_upload_auth_api" {
  bucket = "frontend-dev-bucket-for-sam"
  key    = "auth-${local.environment_name_slug}.zip"
  source = data.archive_file.source_auth_api.output_path
}
resource "aws_cloudformation_stack" "deploy_auth_api" {
  name = "${local.environment_name_slug}-auth"
  capabilities = ["CAPABILITY_IAM", "CAPABILITY_AUTO_EXPAND"]
  on_failure = "DELETE"
  depends_on = [null_resource.services_npm_recursive_build ]
  parameters = {
        AppEnvironment = local.environment_name_slug
        ParamAWSID = var.aws_access_key_id
        ParamAWSKey = var.aws_secret_access_key
        ParamAWSRegion = var.aws_default_region
        ParamApiVersion = var.api_version_number
        ParamCognitoAppId = aws_cognito_user_pool_client.this.id
        ParamCognitoPool = aws_cognito_user_pool.this.id
        ParamDBHost = var.rds_db_host
        ParamDBPassword = var.rds_db_password
        ParamDBUsername = var.rds_db_username
        ParamEmailRedirectUrl = "${aws_s3_bucket.this_public.website_endpoint}/sign-up"
        ParamForgotPwdRedirect = "${aws_s3_bucket.this_public.website_endpoint}/reset-password"
        ParamS3PrivateBucket = aws_s3_bucket.this_private.id
        ParamS3PublicBucket = aws_s3_bucket.this_assets.id
        ParamVPCPrivateSubnet1 = var.vpc_private_subnet1
        ParamVPCPrivateSubnet2 = var.vpc_private_subnet2
        ParamVPCSG = var.vpc_sg
  }
  template_body = <<STACK
{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Transform": "AWS::Serverless-2016-10-31",
    "Description": "sx-mdc-service-auth\n",
    "Globals": {
        "Function": {
            "Timeout": 90,
            "VpcConfig": {
                "SecurityGroupIds": [
                    {
                        "Ref": "ParamVPCSG"
                    }
                ],
                "SubnetIds": [
                    {
                        "Ref": "ParamVPCPrivateSubnet1"
                    },
                    {
                        "Ref": "ParamVPCPrivateSubnet2"
                    }
                ]
            },
            "Environment": {
                "Variables": {
                    "API_VERSION": {
                        "Ref": "ParamApiVersion"
                    },
                    "AWS_COGNITO_USER_POOL_ID": {
                        "Ref": "ParamCognitoPool"
                    },
                    "AWS_COGNITO_CLIENT_APP_ID": {
                        "Ref": "ParamCognitoAppId"
                    },
                    "AWS_S3_ACCESS_KEY_ID": {
                        "Ref": "ParamAWSID"
                    },
                    "AWS_S3_SECRET_ACCESS_KEY": {
                        "Ref": "ParamAWSKey"
                    },
                    "AWS_S3_REGION": {
                        "Ref": "ParamAWSRegion"
                    },
                    "AWS_S3_PRIVATE_BUCKET": {
                        "Ref": "ParamS3PrivateBucket"
                    },
                    "AWS_S3_PUBLIC_BUCKET": {
                        "Ref": "ParamS3PublicBucket"
                    },
                    "POSTGRES_HOST": {
                        "Ref": "ParamDBHost"
                    },
                    "POSTGRES_USER": {
                        "Ref": "ParamDBUsername"
                    },
                    "POSTGRES_PWD": {
                        "Ref": "ParamDBPassword"
                    },
                    "PREFIX_COMMON_LIB": "/opt",
                    "MIGRATIONS_DIR": "/opt/rdb-migration/migrations",
                    "COMPANY_REGISTRY_TABLE_NAME": {
                        "Fn::Sub": "company-registry-${local.environment_name_slug}"
                    },
                    "ID_TOKEN_TTL": {
                        "Ref": "ParamIdTokenTtl"
                    },
                    "ACCESS_TOKEN_TTL": {
                        "Ref": "ParamAccessTokenTtl"
                    },
                    "REFRESH_TOKEN_TTL": {
                        "Ref": "ParamRefreshTokenTtl"
                    }
                }
            }
        }
    },
    "Parameters": {
        "ParamApiVersion": {
            "Type": "String",
            "Default": "0.0.0"
        },
        "AppEnvironment": {
            "Type": "String"
        },
        "ParamDBHost": {
            "Type": "String"
        },
        "ParamDBUsername": {
            "Type": "String"
        },
        "ParamDBPassword": {
            "Type": "String"
        },
        "ParamCognitoPool": {
            "Type": "String"
        },
        "ParamCognitoAppId": {
            "Type": "String"
        },
        "ParamEmailRedirectUrl": {
            "Type": "String"
        },
        "ParamForgotPwdRedirect": {
            "Type": "String"
        },
        "ParamS3PublicBucket": {
            "Type": "String"
        },
        "ParamS3PrivateBucket": {
            "Type": "String"
        },
        "ParamVPCSG": {
            "Type": "String"
        },
        "ParamVPCPrivateSubnet1": {
            "Type": "String"
        },
        "ParamVPCPrivateSubnet2": {
            "Type": "String"
        },
        "ParamAWSRegion": {
            "Type": "String"
        },
        "ParamAWSID": {
            "Type": "String"
        },
        "ParamAWSKey": {
            "Type": "String"
        },
        "ParamIdTokenTtl": {
            "Type": "String",
            "Default": 1
        },
        "ParamAccessTokenTtl": {
            "Type": "String",
            "Default": 1
        },
        "ParamRefreshTokenTtl": {
            "Type": "String",
            "Default": 30
        }
    },
    "Resources": {
        "MDCAuthApi": {
            "Type": "AWS::Serverless::Api",
            "Properties": {
                "EndpointConfiguration": "REGIONAL",
                "StageName": "api",
                "Cors": {
                    "AllowOrigin": "'*'",
                    "AllowMethods": "'OPTIONS,HEAD,GET,PUT,POST,PATCH,DELETE'",
                    "AllowHeaders": "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,x-id-token,x-subdomain'"
                }
            }
        },
        "InviteUserFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_auth_api.key}"
                },
                "Handler": "InviteUser.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "InviteUser": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/auth/invite-user",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCAuthApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonCognitoPowerUser",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    {
                        "DynamoDBCrudPolicy": {
                            "TableName": "${aws_dynamodb_table.this_company_registry_table.name}"
                        }
                    }
                 ]
            }
        },
       "ReinviteUserFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_auth_api.key}"
                },
                "Handler": "ReinviteUser.handler",
                "Layers": [
                   "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "InviteUser": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/auth/reinvite-user",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCAuthApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonCognitoPowerUser",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                  {
                    "DynamoDBCrudPolicy": {
                        "TableName": "${aws_dynamodb_table.this_company_registry_table.name}"

                    }
                  }
                ]
            }
       },
        "InviteUsersFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_auth_api.key}"
                },
                "Handler": "InviteUsers.handler",
                "Layers": [
                   "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "InviteUsers": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/auth/invite-users",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCAuthApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonCognitoPowerUser",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                  {
                    "DynamoDBCrudPolicy": {
                      "TableName": "${aws_dynamodb_table.this_company_registry_table.name}"
                    }
                  }
                ]
            }
        },
        "SignUpFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_auth_api.key}"
                },
                "Handler": "SignUp.handler",
                "Layers": [
                    "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "SignUp": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/auth/sign-up",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCAuthApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonCognitoPowerUser",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    {
                        "DynamoDBCrudPolicy": {
                            "TableName": "${aws_dynamodb_table.this_company_registry_table.name}"
                            }
                        }
                ]
            }
        },
        "LogInFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_auth_api.key}"
                },
                "Handler": "LogIn.handler",
                "Layers": [
                   "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "LogIn": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/auth/log-in",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCAuthApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonCognitoPowerUser",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    {
                        "DynamoDBCrudPolicy": {
                            "TableName": "${aws_dynamodb_table.this_company_registry_table.name}"
                            }
                        }
                ]
            }
        },
        "LogOutFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_auth_api.key}"
                },
                "Handler": "LogOut.handler",
                "Layers": [
                   "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "LogOut": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/auth/log-out",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCAuthApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonCognitoPowerUser",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    {
                        "DynamoDBCrudPolicy": {
                            "TableName": "${aws_dynamodb_table.this_company_registry_table.name}"
                            }
                        }
                ]
            }
        },
        "ForgotPwdFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_auth_api.key}"
                },
                "Handler": "ForgotPwd.handler",
                "Layers": [
                   "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "ForgortPwd": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/auth/forgot-pwd",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCAuthApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonCognitoPowerUser",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    {
                        "DynamoDBCrudPolicy": {
                            "TableName": "${aws_dynamodb_table.this_company_registry_table.name}"
                            }
                        }
                ]
            }
        },
        "ResetPwdFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_auth_api.key}"
                },
                "Handler": "ResetPwd.handler",
                "Layers": [
                    "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "ResetPwd": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/auth/reset-pwd",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCAuthApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonCognitoPowerUser",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    {
                        "DynamoDBCrudPolicy": {
                            "TableName": "${aws_dynamodb_table.this_company_registry_table.name}"
                            }
                        }
                ]
            }
        },
        "RefreshTokenFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_auth_api.key}"
                },
                "Handler": "RefreshToken.handler",
                "Layers": [
                    "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "RefreshToken": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/auth/refresh-token",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCAuthApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonCognitoPowerUser",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    {
                        "DynamoDBCrudPolicy": {
                            "TableName": "${aws_dynamodb_table.this_company_registry_table.name}"
                            }
                        }
                ]
            }
        },
        "DeleteUserFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_auth_api.key}"
                },
                "Handler": "DeleteUser.handler",
                "Layers": [
                    "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "SignUp": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/auth/delete-user",
                            "Method": "delete",
                            "RestApiId": {
                                "Ref": "MDCAuthApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonCognitoPowerUser",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    {
                        "DynamoDBCrudPolicy": {
                            "TableName": "${aws_dynamodb_table.this_company_registry_table.name}"
                            }
                        }
                ]
            }
        },
        "CustomMessageFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_common_layer.key}"
                },
                "Handler": "CustomMessage.handler",
                "Runtime": "nodejs12.x",
                "Environment": {
                    "Variables": {
                        "INVITE_EMAIL_REDIRECT_URL": {
                            "Ref": "ParamEmailRedirectUrl"
                        },
                        "FORGOT_PWD_EMAIL_REDIRECT_URL": {
                            "Ref": "ParamForgotPwdRedirect"
                        },
                        "PREFIX_COMMON_LIB": "/opt"
                    }
                },
                "Policies": [
                    "AmazonCognitoPowerUser",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    {
                        "DynamoDBCrudPolicy": {
                            "TableName": "${aws_dynamodb_table.this_company_registry_table.name}"
                            }
                        }
                ]
            }
        },
        "CustomMessageUserpoolPermission": {
            "Type": "AWS::Lambda::Permission",
            "Properties": {
                "FunctionName": {
                    "Fn::GetAtt": [
                        "CustomMessageFunction",
                        "Arn"
                    ]
                },
                "Action": "lambda:InvokeFunction",
                "Principal": "cognito-idp.amazonaws.com",
                "SourceArn": {
                    "Fn::Sub": "arn:aws:cognito-idp:${var.aws_default_region}:${var.aws_account_id}:userpool/*"
                }
            }
        },
        "MigrateCognitoUserFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_auth_api.key}"
                },
                "Handler": "MigrateCognitoUser.handler",
                "Runtime": "nodejs12.x",
                "Timeout": 240,
                "Layers": [
                    "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Environment": {
                    "Variables": {
                        "INVITE_EMAIL_REDIRECT_URL": {
                            "Ref": "ParamEmailRedirectUrl"
                        },
                        "FORGOT_PWD_EMAIL_REDIRECT_URL": {
                            "Ref": "ParamForgotPwdRedirect"
                        }
                    }
                },
                "Policies": [
                    "AmazonCognitoPowerUser",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    {
                        "DynamoDBCrudPolicy": {
                            "TableName": "${aws_dynamodb_table.this_company_registry_table.name}"
                            }
                        }
                ]
            }
        },
        "MigrateCognitoUserPermission": {
            "Type": "AWS::Lambda::Permission",
            "Properties": {
                "FunctionName": {
                    "Fn::GetAtt": [
                        "MigrateCognitoUserFunction",
                        "Arn"
                    ]
                },
                "Action": "lambda:InvokeFunction",
                "Principal": "cognito-idp.amazonaws.com",
                "SourceArn": {
                    "Fn::Sub": "arn:aws:cognito-idp:${var.aws_default_region}:${var.aws_account_id}:userpool/*"
                }
            }
        },
        "InviteSystemAdminFunction1": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_auth_api.key}"
                },
                "Handler": "InviteSystemAdmin.handler",
                "Layers": [
                    "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "InviteUser": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/auth/invite-system-admin",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCAuthApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonCognitoPowerUser",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    {
                        "DynamoDBCrudPolicy": {
                            "TableName": "${aws_dynamodb_table.this_company_registry_table.name}"
                            }
                        }
                ]
            }
        },
        "AccountExistsFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_auth_api.key}"
                },
                "Handler": "AccountExists.handler",
                "Layers": [
                    "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "Api": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/auth/account-exists",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCAuthApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonCognitoPowerUser",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    {
                        "DynamoDBCrudPolicy": {
                            "TableName": "${aws_dynamodb_table.this_company_registry_table.name}"
                            }
                        }
                ]
            }
        }
    },
    "Outputs": {
        "MDCAuthApi": {
            "Description": "Auth API endpoint URL",
            "Value": { "Fn::Sub": "https://$${MDCAuthApi}.execute-api.us-eat-1.amazonaws.com/api/" }

        },
        "CustomLambdaFuctionARN": {
             "Description": "Custom message function Lambda ARN",
             "Value": {  "Fn::GetAtt": ["CustomMessageFunction","Arn"] }
        }
    }
}
STACK
}

