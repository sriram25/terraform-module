data "archive_file" "source_listview_api" {
  type        = "zip"
  source_dir  = "${local.base_code_path}/../services/listview/src/"
  output_path = "/tmp/listview-${local.environment_name_slug}.zip"
  depends_on = [null_resource.services_npm_recursive_build ]
}
resource "aws_s3_bucket_object" "file_upload_listview_api" {
  bucket = "frontend-dev-bucket-for-sam"
  key    = "listview-${local.environment_name_slug}.zip"
  source = data.archive_file.source_listview_api.output_path
}
resource "aws_cloudformation_stack" "deploy_listview_api" {
  name = "${local.environment_name_slug}-listview"
  capabilities = [
    "CAPABILITY_IAM",
    "CAPABILITY_AUTO_EXPAND"]
  on_failure = "DELETE"
  depends_on = [
    null_resource.services_npm_recursive_build]
  parameters = {
    AppEnvironment = local.environment_name_slug
    ParamAWSID = var.aws_access_key_id
    ParamAWSKey = var.aws_secret_access_key
    ParamAWSRegion = var.aws_default_region
    ParamApiVersion = var.api_version_number
    ParamCognitoAppId = aws_cognito_user_pool_client.this.id
    ParamCognitoPool = aws_cognito_user_pool.this.id
    ParamDBHost = var.rds_db_host
    ParamDBPassword = var.rds_db_password
    ParamDBUsername = var.rds_db_username
    ParamS3PrivateBucket = aws_s3_bucket.this_private.id
    ParamS3PublicBucket = aws_s3_bucket.this_assets.id
    ParamVPCPrivateSubnet1 = var.vpc_private_subnet1
    ParamVPCPrivateSubnet2 = var.vpc_private_subnet2
    ParamVPCSG = var.vpc_sg
  }
  template_body = <<STACK
{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Transform": "AWS::Serverless-2016-10-31",
    "Description": "sx-mdc-service-listview",
    "Globals": {
        "Function": {
            "Timeout": 10,
            "Environment": {
                "Variables": {
                    "API_VERSION": {
                        "Ref": "ParamApiVersion"
                    },
                    "AWS_S3_ACCESS_KEY_ID": {
                        "Ref": "ParamAWSID"
                    },
                    "AWS_S3_SECRET_ACCESS_KEY": {
                        "Ref": "ParamAWSKey"
                    },
                    "AWS_S3_REGION": {
                        "Ref": "ParamAWSRegion"
                    },
                    "AWS_S3_PRIVATE_BUCKET": {
                        "Ref": "ParamS3PrivateBucket"
                    },
                    "AWS_S3_PUBLIC_BUCKET": {
                        "Ref": "ParamS3PublicBucket"
                    },
                    "POSTGRES_HOST": {
                        "Ref": "ParamDBHost"
                    },
                    "POSTGRES_USER": {
                        "Ref": "ParamDBUsername"
                    },
                    "POSTGRES_PWD": {
                        "Ref": "ParamDBPassword"
                    },
                    "PREFIX_COMMON_LIB": "/opt",
                    "MIGRATIONS_DIR": "/opt/rdb-migration/migrations",
                    "COMPANY_REGISTRY_TABLE_NAME": "${aws_dynamodb_table.this_company_registry_table.name}",
                    "ID_TOKEN_TTL": {
                        "Ref": "ParamIdTokenTtl"
                    },
                    "ACCESS_TOKEN_TTL": {
                        "Ref": "ParamAccessTokenTtl"
                    },
                    "REFRESH_TOKEN_TTL": {
                        "Ref": "ParamRefreshTokenTtl"
                    }
                }
            }
        }
    },
    "Parameters": {
        "ParamApiVersion": {
            "Type": "String",
            "Default": "0.0.0"
        },
        "AppEnvironment": {
            "Type": "String"
        },
        "ParamDBHost": {
            "Type": "String"
        },
        "ParamDBUsername": {
            "Type": "String"
        },
        "ParamDBPassword": {
            "Type": "String"
        },
        "ParamCognitoPool": {
            "Type": "String"
        },
        "ParamCognitoAppId": {
            "Type": "String"
        },
        "ParamVPCPrivateSubnet1": {
            "Type": "String"
        },
        "ParamVPCPrivateSubnet2": {
            "Type": "String"
        },
        "ParamVPCSG": {
            "Type": "String"
        },
        "ParamAWSRegion": {
            "Type": "String"
        },
        "ParamAWSID": {
            "Type": "String"
        },
        "ParamAWSKey": {
            "Type": "String"
        },
        "ParamS3PrivateBucket": {
            "Type": "String"
        },
        "ParamS3PublicBucket": {
            "Type": "String"
        },
        "ParamIdTokenTtl": {
            "Type": "String",
            "Default": 1
        },
        "ParamAccessTokenTtl": {
            "Type": "String",
            "Default": 1
        },
        "ParamRefreshTokenTtl": {
            "Type": "String",
            "Default": 30
        }
    },
    "Resources": {
        "MDCListViewApi": {
            "Type": "AWS::Serverless::Api",
            "Properties": {
                "StageName": "api",
                "EndpointConfiguration": "REGIONAL",
                "Cors": {
                    "AllowOrigin": "'*'",
                    "AllowMethods": "'OPTIONS,HEAD,GET,PUT,POST,PATCH,DELETE'",
                    "AllowHeaders": "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,x-id-token,x-file-path,x-project-id,x-datetime,x-page,x-page-size,x-sort'"
                }
            }
        },
        "GetListViewOwners": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_listview_api.key}"
                },
                "Handler": "GetListViewOwnersForProject.handler",
                "Layers": [
                    "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetListViewOwners": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/listview/get-listview-owners",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCListViewApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "GetListView": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_listview_api.key}"
                },
                "Handler": "GetListView.handler",
                "Layers": [
                    "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetListViewOwners": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/listview/get-listview",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCListViewApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "DeleteListViewItems": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_listview_api.key}"
                },
                "Handler": "DeleteListViewItems.handler",
                "Layers": [
                    "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetListViewOwners": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/listview/delete-listview-items",
                            "Method": "delete",
                            "RestApiId": {
                                "Ref": "MDCListViewApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        }
    },
    "Outputs": {
        "MDCListviewApi": {
            "Description": "Listview API endpoint URL",
            "Value": { "Fn::Sub": "https://$${MDCListViewApi}.execute-api.us-eat-1.amazonaws.com/api/" }

        }
    }
}
STACK
}