data "archive_file" "source_common_layer" {
  type        = "zip"
  source_dir  = "${local.base_code_path}/../services/common/src/"
  output_path = "/tmp/common-${local.environment_name_slug}.zip"
  depends_on = [null_resource.services_npm_recursive_build ]
}
resource "aws_s3_bucket_object" "file_upload_common_layer" {
  bucket = "frontend-dev-bucket-for-sam"
  key    = "common-${local.environment_name_slug}.zip"
  source = data.archive_file.source_common_layer.output_path
}
resource "aws_cloudformation_stack" "deploy_common_layer" {
  name = "${local.environment_name_slug}-common"
  capabilities = ["CAPABILITY_IAM", "CAPABILITY_AUTO_EXPAND"]
  on_failure = "DELETE"
  depends_on = [null_resource.services_npm_recursive_build ]
  template_body = <<STACK
{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Transform": "AWS::Serverless-2016-10-31",
    "Description": "SAM Template for common",
    "Resources": {
        "MDCServiceLayer": {
            "Type": "AWS::Serverless::LayerVersion",
            "Properties": {
                "Description": "Dependencies and middlewares for sam app",
                "ContentUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_common_layer.key}"
                },
                "CompatibleRuntimes": [
                    "nodejs12.x"
                ]
            }
        }
    },
    "Outputs": {
        "MDCServiceLayer": {
            "Description": "The MDCServiceLayer",
            "Export": {
                "Name": "${local.environment_name_slug}-auth-mdcservicelayer"
            },
            "Value" : {
              "Ref" : "MDCServiceLayer"
            }
        }
    }
}
STACK
}