output "aws_cognito_user_pool_default_id" {
  value = aws_cognito_user_pool.this.id
}
output "aws_cognito_user_pool_client_default_app_id" {
  value = aws_cognito_user_pool_client.this.id
}
output "dynamodb_registry_table_name" {
  value = aws_dynamodb_table.this_company_registry_table.name
}
output "dynamodb_default_table_stream_arn" {
    value = aws_dynamodb_table.this_default_dynamodb_table.stream_arn
}
output "frontend_url" {
  value = aws_s3_bucket.this_public.website_endpoint
}