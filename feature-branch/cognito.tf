resource "aws_cognito_user_pool" "this" {
    name = "default-${local.environment_name_slug}"
    password_policy {
        minimum_length = 6
        require_lowercase = true
        require_numbers = true
        require_symbols = false
        require_uppercase = true
        temporary_password_validity_days = 7
    }
    lambda_config {
        custom_message = var.custom_message_lambda_arn
    }
    schema {
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        name = "schema"
        string_attribute_constraints {
            max_length = "256"
            min_length = "1"
        }
        required = false
    }
    schema {
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        name = "invitedBy"
        string_attribute_constraints {
            max_length = "256"
            min_length = "1"
        }
        required = false
    }
    schema {
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        name = "roleId"
        string_attribute_constraints {
            max_length = "256"
            min_length = "1"
        }
        required = false
    }
    schema {
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        name = "accountId"
        string_attribute_constraints {
            max_length = "256"
            min_length = "1"
        }
        required = false
    }
    schema {
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        name = "companyName"
        string_attribute_constraints {
            max_length = "256"
            min_length = "1"
        }
        required = false
    }
    schema {
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        name = "companyType"
        string_attribute_constraints {
            max_length = "256"
            min_length = "1"
        }
        required = false
    }
    schema {
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        name = "userId"
        string_attribute_constraints {
            max_length = "256"
            min_length = "1"
        }
        required = false
    }
    schema {
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        name = "userPoolId"
        string_attribute_constraints {
            max_length = "256"
            min_length = "1"
        }
        required = false
    }
    schema {
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        name = "clientAppId"
        string_attribute_constraints {
            max_length = "256"
            min_length = "1"
        }
        required = false
    }
    auto_verified_attributes = [
        "email"
    ]
    username_attributes = [
        "email"
    ]
    sms_verification_message = "Your verification code is {####}. "
    email_verification_message = "Your verification code is {####}. "
    email_verification_subject = "Your verification code"
    sms_authentication_message = "Your authentication code is {####}. "
    mfa_configuration = "OFF"
    email_configuration {

    }
    admin_create_user_config {
        allow_admin_create_user_only = false
        invite_message_template {
            email_message = "Your username is {username} and temporary password is {####}. "
            email_subject = "Your temporary password"
            sms_message = "Your username is {username} and temporary password is {####}. "
        }
    }
    lifecycle {
    ignore_changes = [
      lambda_config,
      tags
    ]
  }
}
resource "aws_cognito_user_pool_client" "this" {
    user_pool_id = aws_cognito_user_pool.this.id
    name = "default-${local.environment_name_slug}"
    refresh_token_validity = 30
    read_attributes = [
        "custom:accountId",
        "custom:clientAppId",
        "custom:companyName",
        "custom:companyType",
        "custom:invitedBy",
        "custom:roleId",
        "custom:schema",
        "custom:userId",
        "custom:userPoolId",
        "email",
        "gender",
        "name",
        "phone_number"
    ]
    write_attributes = [
        "custom:accountId",
        "custom:clientAppId",
        "custom:companyName",
        "custom:companyType",
        "custom:invitedBy",
        "custom:roleId",
        "custom:schema",
        "custom:userId",
        "custom:userPoolId",
        "email",
        "gender",
        "name",
        "phone_number"
    ]
    explicit_auth_flows = [
        "ALLOW_ADMIN_USER_PASSWORD_AUTH",
        "ALLOW_CUSTOM_AUTH",
        "ALLOW_REFRESH_TOKEN_AUTH",
        "ALLOW_USER_SRP_AUTH"
    ]
}