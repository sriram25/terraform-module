/*
resource "null_resource" "lint_coverage" {
  provisioner "local-exec" {
    command = "cd ../services &  export PREFIX_COMMON_LIB=${var.PREFIX_COMMON_LIB} && npm run lint && npm run coverage"
  }
    triggers = {
    always_run = timestamp()
  }
}
data "null_data_source" "wait_for_lint_test_build" {
  inputs = {
    build_dependency_id = null_resource.lint_coverage.id
    source_dir           = "${path.module}/src/"
  }
}
*/
resource "null_resource" "services_npm_recursive_build" {
  provisioner "local-exec" {
    command = "cd ../services && echo $PWD && npm-recursive-install"
  }
      triggers = {
    always_run = timestamp()
  }
  depends_on = [
    aws_cognito_user_pool.this,
    aws_cognito_user_pool_client.this,
    aws_dynamodb_table.this_company_registry_table,
    aws_dynamodb_table.this_default_dynamodb_table,
    aws_s3_bucket.this_assets,
    aws_s3_bucket.this_public,
    aws_s3_bucket.this_private,
    null_resource.create-schema,
    null_resource.create-schema-role
  ]
}
resource "null_resource" "resources_npm_recursive_build" {
  provisioner "local-exec" {
    command = "cd ../Resources && echo $PWD && npm-recursive-install"
  }
      triggers = {
    always_run = timestamp()
  }
  depends_on = [
    aws_cognito_user_pool.this,
    aws_cognito_user_pool_client.this,
    aws_dynamodb_table.this_company_registry_table,
    aws_dynamodb_table.this_default_dynamodb_table,
    aws_s3_bucket.this_assets,
    aws_s3_bucket.this_public,
    aws_s3_bucket.this_private,
    null_resource.create-schema,
    null_resource.create-schema-role
    ]
}