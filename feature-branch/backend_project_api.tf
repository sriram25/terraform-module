data "archive_file" "source_project_api" {
  type        = "zip"
  source_dir  = "${local.base_code_path}/../services/project/src/"
  output_path = "/tmp/project-${local.environment_name_slug}.zip"
  depends_on = [null_resource.services_npm_recursive_build ]
}
resource "aws_s3_bucket_object" "file_upload_project_api" {
  bucket = "frontend-dev-bucket-for-sam"
  key    = "project-${local.environment_name_slug}.zip"
  source = data.archive_file.source_project_api.output_path
}
resource "aws_cloudformation_stack" "deploy_project_api" {
  name = "${local.environment_name_slug}-project"
  capabilities = ["CAPABILITY_IAM", "CAPABILITY_AUTO_EXPAND"]
  on_failure = "DELETE"
  depends_on = [null_resource.services_npm_recursive_build ]
  parameters = {
        AppEnvironment = local.environment_name_slug
        ParamAWSID = var.aws_access_key_id
        ParamAWSKey = var.aws_secret_access_key
        ParamAWSRegion = var.aws_default_region
        ParamApiVersion = var.api_version_number
        ParamCognitoAppId = aws_cognito_user_pool_client.this.id
        ParamCognitoPool = aws_cognito_user_pool.this.id
        ParamDBHost = var.rds_db_host
        ParamDBPassword = var.rds_db_password
        ParamDBUsername = var.rds_db_username
        ParamEmailRedirectUrl = "${aws_s3_bucket.this_public.website_endpoint}/sign-up"
        ParamForgotPwdRedirect = "${aws_s3_bucket.this_public.website_endpoint}/reset-password"
        ParamS3PrivateBucket = aws_s3_bucket.this_private.id
        ParamS3PublicBucket = aws_s3_bucket.this_assets.id
        ParamVPCPrivateSubnet1 = var.vpc_private_subnet1
        ParamVPCPrivateSubnet2 = var.vpc_private_subnet2
        ParamAWSRecipientEmail = var.recipient_email
        ParamAWSSenderEmail = var.sender_email
        ParamAwsNotificationTableName = aws_dynamodb_table.this_default_dynamodb_table.name
        ParamVPCSG = var.vpc_sg
  }
  template_body = <<STACK
{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Transform": "AWS::Serverless-2016-10-31",
    "Description": "sx-mdc-service-project",
    "Parameters": {
        "ParamApiVersion": {
            "Type": "String",
            "Default": "0.0.0"
        },
        "AppEnvironment": {
            "Type": "String"
        },
        "ParamDBHost": {
            "Type": "String"
        },
        "ParamDBUsername": {
            "Type": "String"
        },
        "ParamDBPassword": {
            "Type": "String"
        },
        "ParamCognitoPool": {
            "Type": "String"
        },
        "ParamCognitoAppId": {
            "Type": "String"
        },
        "ParamEmailRedirectUrl": {
            "Type": "String"
        },
        "ParamForgotPwdRedirect": {
            "Type": "String"
        },
        "ParamS3PublicBucket": {
            "Type": "String"
        },
        "ParamS3PrivateBucket": {
            "Type": "String"
        },
        "ParamVPCPrivateSubnet1": {
            "Type": "String"
        },
        "ParamVPCPrivateSubnet2": {
            "Type": "String"
        },
        "ParamVPCSG": {
            "Type": "String"
        },
        "ParamAWSRegion": {
            "Type": "String"
        },
        "ParamAWSID": {
            "Type": "String"
        },
        "ParamAWSKey": {
            "Type": "String"
        },
        "ParamAWSSenderEmail": {
            "Type": "String"
        },
        "ParamAWSRecipientEmail": {
            "Type": "String"
        },
        "ParamAwsNotificationTableName": {
            "Type": "String"
        },
        "ParamIdTokenTtl": {
            "Type": "String",
            "Default": 1
        },
        "ParamAccessTokenTtl": {
            "Type": "String",
            "Default": 1
        },
        "ParamRefreshTokenTtl": {
            "Type": "String",
            "Default": 30
        }
    },
    "Globals": {
        "Function": {
            "Timeout": 10,
            "Environment": {
                "Variables": {
                    "API_VERSION": {
                        "Ref": "ParamApiVersion"
                    },
                    "AWS_S3_ACCESS_KEY_ID": {
                        "Ref": "ParamAWSID"
                    },
                    "AWS_S3_SECRET_ACCESS_KEY": {
                        "Ref": "ParamAWSKey"
                    },
                    "AWS_S3_REGION": {
                        "Ref": "ParamAWSRegion"
                    },
                    "AWS_S3_PRIVATE_BUCKET": {
                        "Ref": "ParamS3PrivateBucket"
                    },
                    "AWS_S3_PUBLIC_BUCKET": {
                        "Ref": "ParamS3PublicBucket"
                    },
                    "POSTGRES_HOST": {
                        "Ref": "ParamDBHost"
                    },
                    "POSTGRES_USER": {
                        "Ref": "ParamDBUsername"
                    },
                    "POSTGRES_PWD": {
                        "Ref": "ParamDBPassword"
                    },
                    "PREFIX_COMMON_LIB": "/opt",
                    "MIGRATIONS_DIR": "/opt/rdb-migration/migrations",
                    "COMPANY_REGISTRY_TABLE_NAME": "company-registry-${local.environment_name_slug}",
                    "ID_TOKEN_TTL": {
                        "Ref": "ParamIdTokenTtl"
                    },
                    "ACCESS_TOKEN_TTL": {
                        "Ref": "ParamAccessTokenTtl"
                    },
                    "REFRESH_TOKEN_TTL": {
                        "Ref": "ParamRefreshTokenTtl"
                    },
                    "AWS_DYNAMO_NOTIFICATION_TABLE": {
                            "Ref": "ParamAwsNotificationTableName"
                    }
                }
            },
            "VpcConfig": {
                    "SecurityGroupIds": [
                        {
                            "Ref": "ParamVPCSG"
                        }
                    ],
                    "SubnetIds": [
                        {
                            "Ref": "ParamVPCPrivateSubnet1"
                        },
                        {
                            "Ref": "ParamVPCPrivateSubnet2"
                        }
                    ]
                }
        }
    },
    "Resources": {
        "MDCProjectApi": {
            "Type": "AWS::Serverless::Api",
            "Properties": {
                "EndpointConfiguration": "REGIONAL",
                "StageName": "api",
                "Cors": {
                    "AllowOrigin": "'*'",
                    "AllowMethods": "'OPTIONS,HEAD,GET,PUT,POST,PATCH,DELETE'",
                    "AllowHeaders": "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,x-id-token,x-file-path,x-project-id,x-datetime,x-task-id,x-subtask-id,x-vendor-delivery-id,x-prefab-delivery-id,x-project-team-id,x-resource-type,x-resource-id,x-document-id,x-user-id'"
                }
            }
        },
        "UploadProjectImageFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "UploadProjectImage.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "UploadProjectImage": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/upload-project-image",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonS3FullAccess"
                ]
            }
        },
        "DownloadProjectImageFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "DownloadProjectImage.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "DownloadProjectImage": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/download-project-image",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonS3ReadOnlyAccess"
                ]
            }
        },
        "UploadTradeScheduleFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "UploadTradeSchedule.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "UploadTradeSchedule": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/upload-trade-schedule",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Environment": {
                    "Variables": {
                        "AWS_SES_S3_NOTIFICATION_SENDER_EMAIL": {
                            "Ref": "ParamAWSSenderEmail"
                        },
                        "AWS_SES_S3_NOTIFICATION_RECIPIENT_EMAIL": {
                            "Ref": "ParamAWSRecipientEmail"
                        }
                    }
                },
                "Policies": [
                    "AmazonSESFullAccess",
                    "AmazonS3FullAccess",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "DownloadTradeScheduleFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "DownloadTradeSchedule.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "DownloadTradeSchedule": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/download-trade-schedule",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonS3ReadOnlyAccess"
                ]
            }
        },
        "CreateProjectFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "CreateProject.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "CreateProject": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/create-project",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonS3FullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonDynamoDBFullAccess"
                ]
            }
        },
        "CreateCustomTradeFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "CreateCustomTrade.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "CreateProject": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/create-custom-trade",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "UpdateProjectFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "UpdateProject.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "UpdateProject": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/update-project",
                            "Method": "put",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Environment": {
                    "Variables": {
                        "AWS_DYNAMO_NOTIFICATION_TABLE": {
                            "Ref": "ParamAwsNotificationTableName"
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonDynamoDBFullAccess"
                ]
            }
        },
        "GetProjectFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "GetProject.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetProject": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/get-project",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "GetProjectsFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "GetProjects.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetProjects": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/get-projects",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "CreateTaskFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "CreateTask.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetTasks": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/create-task",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Environment": {
                    "Variables": {
                        "AWS_DYNAMO_NOTIFICATION_TABLE": {
                            "Ref": "ParamAwsNotificationTableName"
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "CreateTasksFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "CreateTasks.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetTasks": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/create-tasks",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "UpdateTaskFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "UpdateTask.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetTasks": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/update-task",
                            "Method": "put",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonDynamoDBFullAccess"
                ]
            }
        },
        "UpdateTasksFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "UpdateTasks.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetTasks": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/update-tasks",
                            "Method": "put",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonDynamoDBFullAccess"
                ]
            }
        },
        "DeleteTaskFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "DeleteTask.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetTasks": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/delete-task",
                            "Method": "delete",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "GetTasksFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "GetTasks.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetTasks": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/get-tasks",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "GetTaskDetailsFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "GetTaskDetails.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetTasks": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/get-task-details",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Environment": {
                    "Variables": {
                        "DB_MAX_POOL_SIZE": 3
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "CreateSubtaskFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "CreateSubtask.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "createSubtasks": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/create-subtask",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Environment": {
                    "Variables": {
                        "AWS_DYNAMO_NOTIFICATION_TABLE": {
                            "Ref": "ParamAwsNotificationTableName"
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "UpdateSubtaskFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "UpdateSubtask.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "UpdateSubtasks": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/update-subtask",
                            "Method": "put",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "DeleteSubtaskFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "DeleteSubtask.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "UpdateSubtasks": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/delete-subtask",
                            "Method": "delete",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "GetSubtasksFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "GetSubtasks.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetSubtasks": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/get-subtasks",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "CreateVendorDeliveryFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "CreateVendorDelivery.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "createVendorDelivery": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/create-vendor-delivery",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonDynamoDBFullAccess"
                ]
            }
        },
        "UpdateVendorDeliveryFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "UpdateVendorDelivery.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "UpdateVendorDelivery": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/update-vendor-delivery",
                            "Method": "put",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonDynamoDBFullAccess"
                ]
            }
        },
        "DeleteVendorDeliveryFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "DeleteVendorDelivery.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "DeleteVendorDelivery": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/delete-vendor-delivery",
                            "Method": "delete",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "GetVendorDeliveriesFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "Timeout": 20,
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "GetVendorDeliveries.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetVendorDeliveries": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/get-vendor-deliveries",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Environment": {
                    "Variables": {
                        "DB_MAX_POOL_SIZE": 4
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "CreatePrefabDeliveryFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "CreatePrefabDelivery.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "createPrefabDelivery": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/create-prefab-delivery",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonDynamoDBFullAccess"
                ]
            }
        },
        "UpdatePrefabDeliveryFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "UpdatePrefabDelivery.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "UpdatePrefabDelivery": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/update-prefab-delivery",
                            "Method": "put",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonDynamoDBFullAccess"
                ]
            }
        },
        "DeletePrefabDeliveryFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "DeletePrefabDelivery.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "DeletePrefabDelivery": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/delete-prefab-delivery",
                            "Method": "delete",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "GetPrefabDeliveriesFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "Timeout": 20,
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "GetPrefabDeliveries.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetPrefabDeliveries": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/get-prefab-deliveries",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Environment": {
                    "Variables": {
                        "DB_MAX_POOL_SIZE": 4
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "GetDeliveriesFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "GetDeliveries.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetTasks": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/get-deliveries",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "CreateDocumentFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "CreateDocument.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "createDocument": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/create-document",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonDynamoDBFullAccess"
                ]
            }
        },
        "UpdateDocumentFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "UpdateDocument.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "UpdateDocument": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/update-document",
                            "Method": "put",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonDynamoDBFullAccess"
                ]
            }
        },
        "DeleteDocumentFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "DeleteDocument.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "DeleteDocument": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/delete-document",
                            "Method": "delete",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "GetDocumentsFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "GetDocuments.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetDocuments": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/get-documents",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "CreateProjectTeamFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "CreateProjectTeam.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "CreateProjectTeam": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/create-project-team",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonDynamoDBFullAccess"
                ]
            }
        },
        "DeleteProjectTeamFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "DeleteProjectTeam.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "DeleteProjectTeam": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/delete-project-team",
                            "Method": "delete",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "GetProjectTeamsFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "GetProjectTeams.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetTasks": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/get-project-teams",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "GetProjectMetricsFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "GetProjectMetrics.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetProjectMetrics": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/get-project-metrics",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Environment": {
                    "Variables": {
                        "DB_MAX_POOL_SIZE": 3
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "SendEmailNotificationFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "SendEmailNotification.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Environment": {
                    "Variables": {
                        "AWS_SES_S3_NOTIFICATION_SENDER_EMAIL": "shirley.hungchoi@gmail.com",
                        "AWS_SES_S3_NOTIFICATION_RECIPIENT_EMAIL": "shirley.hungchoi@gmail.com"
                    }
                },
                "Policies": [
                    "AmazonSESFullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "UpdateTaskStatusFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "UpdateTaskStatusCronJob.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "DailyUpdateEvent": {
                        "Type": "Schedule",
                        "Properties": {
                            "Schedule": "cron(0 9 * * ? *)"
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "UpdateSubTaskStatusFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "UpdateSubTaskStatusCronJob.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "DailyUpdateEvent": {
                        "Type": "Schedule",
                        "Properties": {
                            "Schedule": "cron(0 9 * * ? *)"
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "UpdatePrefabDeliveryStatusFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "UpdatePrefabDeliveryStatusCronJob.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "DailyUpdateEvent": {
                        "Type": "Schedule",
                        "Properties": {
                            "Schedule": "cron(0 9 * * ? *)"
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "UpdateVendorDeliveryStatusFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "UpdateVendorDeliveryStatusCronJob.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "DailyUpdateEvent": {
                        "Type": "Schedule",
                        "Properties": {
                            "Schedule": "cron(0 9 * * ? *)"
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonDynamoDBFullAccess"
                ]
            }
        },
        "GetTaskFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "GetTask.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetTask": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/get-task",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "GetTaskTeamFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "GetTaskTeam.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetTask": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/get-task-team",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "GetProjectTeamFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "GetProjectTeam.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetTask": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/get-project-team",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "UploadManager": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "UploadManager.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "CreateIssue": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/upload",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonS3FullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        },
        "TradesSummaryFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_project_api.key}"
                },
                "Handler": "TradesSummary.handler",
                "Layers": [
                          "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "CreateIssue": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/project/trades-summary",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCProjectApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AmazonS3FullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess"
                ]
            }
        }
    },
    "Outputs": {
        "MDCProjectApi": {
            "Description": "Project API endpoint URL",
            "Value": { "Fn::Sub": "https://$${MDCProjectApi}.execute-api.us-eat-1.amazonaws.com/api/" }
        }
    }
}
STACK
}
