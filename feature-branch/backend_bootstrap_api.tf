data "archive_file" "source_bootstrap_api" {
  type        = "zip"
  source_dir  = "${local.base_code_path}/../services/bootstrap/src/"
  output_path = "/tmp/bootstrap-${local.environment_name_slug}.zip"
  depends_on = [null_resource.services_npm_recursive_build ]
}
resource "aws_s3_bucket_object" "file_upload_bootstrap_api" {
  bucket = "frontend-dev-bucket-for-sam"
  key    = "bootstrap-${local.environment_name_slug}.zip"
  source = data.archive_file.source_bootstrap_api.output_path
}
resource "aws_cloudformation_stack" "deploy_bootstrap_api" {
  name = "${local.environment_name_slug}-bootstrap"
  capabilities = ["CAPABILITY_IAM", "CAPABILITY_AUTO_EXPAND"]
  on_failure = "DELETE"
  depends_on = [null_resource.services_npm_recursive_build ]
  parameters = {
        AppEnvironment = local.environment_name_slug
        ParamAWSID = var.aws_access_key_id
        ParamAWSKey = var.aws_secret_access_key
        ParamAWSRecipientEmail = var.recipient_email
        ParamAWSRegion = var.aws_default_region
        ParamAWSSenderEmail = var.sender_email
        ParamApiVersion = var.api_version_number
        ParamCognitoAppId = aws_cognito_user_pool_client.this.id
        ParamCognitoPool = aws_cognito_user_pool.this.id
        ParamDBHost = var.rds_db_host
        ParamDBPassword = var.rds_db_password
        ParamDBUsername = var.rds_db_username
        ParamEmailRedirectUrl = "${aws_s3_bucket.this_public.website_endpoint}/sign-up"
        ParamForgotPwdRedirect = "${aws_s3_bucket.this_public.website_endpoint}/reset-password"
        ParamS3PrivateBucket = aws_s3_bucket.this_private.id
        ParamS3PublicBucket = aws_s3_bucket.this_assets.id
        ParamVPCPrivateSubnet1 = var.vpc_private_subnet1
        ParamVPCPrivateSubnet2 = var.vpc_private_subnet2
        ParamVPCSG = var.vpc_sg
  }
  template_body = <<STACK
{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Transform": "AWS::Serverless-2016-10-31",
    "Description": "sx-mdc-bootstrap-module\n",
    "Parameters": {
        "AppEnvironment": {
            "Type": "String"
        },
        "ParamDBHost": {
            "Type": "String"
        },
        "ParamDBUsername": {
            "Type": "String"
        },
        "ParamDBPassword": {
            "Type": "String"
        },
        "ParamCognitoPool": {
            "Type": "String"
        },
        "ParamCognitoAppId": {
            "Type": "String"
        },
        "ParamEmailRedirectUrl": {
            "Type": "String"
        },
        "ParamForgotPwdRedirect": {
            "Type": "String"
        },
        "ParamS3PublicBucket": {
            "Type": "String"
        },
        "ParamS3PrivateBucket": {
            "Type": "String"
        },
        "ParamVPCPrivateSubnet1": {
            "Type": "String"
        },
        "ParamVPCPrivateSubnet2": {
            "Type": "String"
        },
        "ParamVPCSG": {
            "Type": "String"
        },
        "ParamAWSRegion": {
            "Type": "String"
        },
        "ParamAWSID": {
            "Type": "String"
        },
        "ParamAWSKey": {
            "Type": "String"
        },
        "ParamAWSSenderEmail": {
            "Type": "String"
        },
        "ParamAWSRecipientEmail": {
            "Type": "String"
        },
        "ParamIdTokenTtl": {
            "Type": "String",
            "Default": 1
        },
        "ParamAccessTokenTtl": {
            "Type": "String",
            "Default": 1
        },
        "ParamRefreshTokenTtl": {
            "Type": "String",
            "Default": 30
        },
        "ParamApiVersion": {
            "Type": "String"
        }
    },
    "Globals": {
        "Function": {
            "Timeout": 240,
            "Runtime": "nodejs12.x",
            "Layers": [
                "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}",
                {
                    "Ref": "PostgresMigrationFilesLayer"
                }
            ],
            "VpcConfig": {
                "SecurityGroupIds": [
                    {
                        "Ref": "ParamVPCSG"
                    }
                ],
                "SubnetIds": [
                    {
                        "Ref": "ParamVPCPrivateSubnet1"
                    },
                    {
                        "Ref": "ParamVPCPrivateSubnet2"
                    }
                ]
            },
            "Environment": {
                "Variables": {
                    "NODE_ENV": {
                        "Ref": "AppEnvironment"
                    },
                    "AWS_ENV_REGION": {
                        "Ref": "ParamAWSRegion"
                    },
                    "AWS_S3_ACCESS_KEY_ID": {
                        "Ref": "ParamAWSID"
                    },
                    "AWS_S3_SECRET_ACCESS_KEY": {
                        "Ref": "ParamAWSKey"
                    },
                    "AWS_S3_REGION": {
                        "Ref": "ParamAWSRegion"
                    },
                    "AWS_S3_PRIVATE_BUCKET": {
                        "Ref": "ParamS3PrivateBucket"
                    },
                    "AWS_S3_PUBLIC_BUCKET": {
                        "Ref": "ParamS3PublicBucket"
                    },
                    "POSTGRES_HOST": {
                        "Ref": "ParamDBHost"
                    },
                    "POSTGRES_USER": {
                        "Ref": "ParamDBUsername"
                    },
                    "POSTGRES_PWD": {
                        "Ref": "ParamDBPassword"
                    },
                    "PREFIX_COMMON_LIB": "/opt",
                    "MIGRATIONS_DIR": "/opt/rdb-migration/migrations",
                    "COMPANY_REGISTRY_TABLE_NAME":
                        "${aws_dynamodb_table.this_company_registry_table.name}",
                    "ID_TOKEN_TTL": {
                        "Ref": "ParamIdTokenTtl"
                    },
                    "ACCESS_TOKEN_TTL": {
                        "Ref": "ParamAccessTokenTtl"
                    },
                    "REFRESH_TOKEN_TTL": {
                        "Ref": "ParamRefreshTokenTtl"
                    }
                }
            }
        }
    },
    "Resources": {
        "PostgresMigrationFilesLayer": {
            "Type": "AWS::Serverless::LayerVersion",
            "Properties": {
                "Description": "Postgres database migration files",
                "ContentUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload.key}"
                },
                "CompatibleRuntimes": [
                    "nodejs12.x"
                ]
            }
        },
        "MDCBootstrapApi": {
            "Type": "AWS::Serverless::Api",
            "Properties": {
                "EndpointConfiguration": "REGIONAL",
                "StageName": "api",
                "Cors": {
                    "AllowOrigin": "'*'",
                    "AllowMethods": "'OPTIONS,HEAD,GET,PUT,POST,DELETE'",
                    "AllowHeaders": "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,x-id-token,x-api-key'"
                }
            }
        },
        "SetupAccountFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_bootstrap_api.key}"
                },
                "Handler": "SetupAccount.handler",
                "Events": {
                    "ApiGatewayEvent": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/setup/account",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCBootstrapApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambda_FullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonCognitoPowerUser",
                    {
                        "DynamoDBCrudPolicy": {
                            "TableName": "${aws_dynamodb_table.this_company_registry_table.name}"
                        }
                    }
                ]
            }
        },
        "UpdateAccountFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_bootstrap_api.key}"
                },
                "Handler": "UpdateAccount.handler",
                "Events": {
                    "ApiGatewayEvent": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/setup/update-account",
                            "Method": "put",
                            "RestApiId": {
                                "Ref": "MDCBootstrapApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambda_FullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonCognitoPowerUser",
                    {
                        "DynamoDBCrudPolicy": {
                            "TableName": "${aws_dynamodb_table.this_company_registry_table.name}"
                        }
                    }
                ]
            }
        },
        "MigrateCompaniesFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_bootstrap_api.key}"
                },
                "Handler": "MigrateCompanies.handler",
                "Events": {
                    "ApiGatewayEvent": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/setup/migrate",
                            "Method": "put",
                            "RestApiId": {
                                "Ref": "MDCBootstrapApi"
                            }
                        }
                    }
                },
                "Policies": [
                    "AWSLambda_FullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonCognitoPowerUser",
                    {
                        "DynamoDBCrudPolicy": {
                            "TableName": "${aws_dynamodb_table.this_company_registry_table.name}"
                        }
                    }
                ]
            }
        }
    },
    "Outputs": {
        "MDCBootstrapApi": {
            "Description": "Bootstrap API endpoint URL",
            "Value": { "Fn::Sub": "https://$${MDCBootstrapApi}.execute-api.us-eat-1.amazonaws.com/api/" }

        }
    }
}
STACK
}