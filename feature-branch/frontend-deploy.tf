resource "null_resource" "frontend_build_deploy" {
  provisioner "local-exec" {
    command = <<COMMANDS
cd ${local.base_code_path}/../client ;
npm-recursive-install ;
export AWS_ACCESS_KEY_ID=${var.aws_access_key_id} ;
export AWS_SECRET_ACCESS_KEY=${var.aws_secret_access_key} ;
export AWS_DEFAULT_REGION=${var.aws_default_region};
export AWS_DEFAULT_OUTPUT=text ;
export REACT_APP_API_AUTH_URL=${aws_cloudformation_stack.deploy_auth_api.outputs.MDCAuthApi};
export REACT_APP_API_USER_URL=${aws_cloudformation_stack.deploy_user_api.outputs.MDCUserApi} ;
export REACT_APP_API_PROJECT_URL=${aws_cloudformation_stack.deploy_project_api.outputs.MDCProjectApi} ;
export REACT_APP_API_ISSUE_URL=${aws_cloudformation_stack.deploy_issue_api.outputs.MDCIssueApi} ;
export REACT_APP_API_NOTIFICATION_URL=${aws_cloudformation_stack.deploy_notifications_api.outputs.MDCNotificationApi} ;
export REACT_APP_API_REMINDER_URL=${aws_cloudformation_stack.deploy_reminder_api.outputs.MDCReminderApi} ;
export REACT_APP_API_LISTVIEW_URL=${aws_cloudformation_stack.deploy_listview_api.outputs.MDCListviewApi}  ;
export REACT_APP_DOMAIN=${aws_s3_bucket.this_public.website_endpoint} ;
npm run-script build && aws s3 sync "${path.root}/../client/build" s3://${aws_s3_bucket.this_public.bucket}/ --acl "public-read" ;
COMMANDS
}
triggers = {
always_run = timestamp()
}
depends_on = [
   aws_s3_bucket.this_public,
   aws_cloudformation_stack.deploy_auth_api,
   aws_cloudformation_stack.deploy_bootstrap_api,
   aws_cloudformation_stack.deploy_common_layer,
   aws_cloudformation_stack.deploy_issue_api,
   aws_cloudformation_stack.deploy_listview_api,
   aws_cloudformation_stack.deploy_notifications_api,
   aws_cloudformation_stack.deploy_project_api,
   aws_cloudformation_stack.deploy_reminder_api,
   aws_cloudformation_stack.deploy_user_api
]
}