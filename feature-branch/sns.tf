resource "aws_sns_topic" "topic_immediate_notification" {
    display_name = ""
    name = "ImmediateNotification"
}
resource "aws_sns_topic_policy" "policy_topic_immediate_notification" {
    policy = <<POLICY_JSON
{
  "Version": "2008-10-17",
  "Id": "__default_policy_ID",
  "Statement": [
    {
      "Sid": "__default_statement_ID",
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": [
        "SNS:GetTopicAttributes",
        "SNS:SetTopicAttributes",
        "SNS:AddPermission",
        "SNS:RemovePermission",
        "SNS:DeleteTopic",
        "SNS:Subscribe",
        "SNS:ListSubscriptionsByTopic",
        "SNS:Publish",
        "SNS:Receive"
      ],
      "Resource": "${aws_sns_topic.topic_immediate_notification.arn}",
      "Condition": {
        "StringEquals": {
          "AWS:SourceOwner": "${var.aws_account_id}"
        }
      }
    }
  ]
}
    POLICY_JSON
    arn = aws_sns_topic.topic_immediate_notification.arn
}
resource "aws_iam_role" "iam_lambda_notification_delivery" {
  name = "iam_lambda_notification_delivery"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}
/*
resource "local_file" "notification_delivery_function_code" {
    content     = <<CONTENT
    exports.handler = async (event) => {
        // TODO implement
        console.log("The SNS messsge", event.Records[0].Sns.Message);
        const response = {
          statusCode: 200,
          body: JSON.stringify('Hello from Lambda!'),
        };
        return response;
    };
    CONTENT
    filename = "${path.module}/notification_delivery_function_code.js"
}
resource "aws_lambda_function" "notification_delivery" {
  handler       = "index.handler"
  filename      = local_file.notification_delivery_function_code.filename
  function_name = "NotificationDelivery"
  runtime       = "nodejs12.x"
  role          = aws_iam_role.iam_lambda_notification_delivery.arn
}
resource "aws_lambda_event_source_mapping" "notification_delivery_event" {
  event_source_arn  = aws_sns_topic.topic_immediate_notification.arn
  function_name     = aws_lambda_function.notification_delivery.arn
  starting_position = "LATEST"
}

resource "aws_sns_topic_subscription" "Subscription" {
    topic_arn = aws_sns_topic.topic_immediate_notification.arn
    endpoint = aws_lambda_function.notification_delivery.arn
    protocol = "lambda"
}
*/
