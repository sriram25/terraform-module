# requires AWS provider
resource "aws_dynamodb_table" "this_company_registry_table" {
    attribute {
        name = "PK"
        type = "S"
    }
    attribute {
        name = "SK"
        type = "S"
    }
    billing_mode = "PAY_PER_REQUEST"
    name = "company-registry-${local.environment_name_slug}"
    hash_key = "PK"
    range_key = "SK"
}
resource "aws_dynamodb_table" "this_default_dynamodb_table" {
    attribute {
        name = "AssignedUserEmail"
        type = "S"
    }
    attribute {
        name = "NotificationId"
        type = "S"
    }
    attribute {
        name = "PK"
        type = "S"
    }
    attribute {
        name = "Project_DateTime"
        type = "S"
    }
    attribute {
        name = "SK"
        type = "S"
    }
    billing_mode = "PAY_PER_REQUEST"
    name = "${local.environment_name_slug}_Notifications"
    hash_key = "PK"
    range_key = "SK"


    global_secondary_index {
        name = "NOTIFICATION_ID_INDEX"
        hash_key = "NotificationId"
        projection_type = "ALL"
        read_capacity = 0
        write_capacity = 0
    }
    global_secondary_index {
        name = "USER_PROJECT_DATETIME_INDEX"
        hash_key = "AssignedUserEmail"
        range_key = "Project_DateTime"
        projection_type = "ALL"
        read_capacity = 0
        write_capacity = 0
    }
    stream_enabled = true
    stream_view_type = "NEW_IMAGE"
}
