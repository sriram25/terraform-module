data "archive_file" "source_notifications_api" {
  type        = "zip"
  source_dir  = "${local.base_code_path}/../services/notifications/src/"
  output_path = "/tmp/notifications-${local.environment_name_slug}.zip"
  depends_on = [null_resource.services_npm_recursive_build ]
}
resource "aws_s3_bucket_object" "file_upload_notifications_api" {
  bucket = "frontend-dev-bucket-for-sam"
  key    = "notifications-${local.environment_name_slug}.zip"
  source = data.archive_file.source_notifications_api.output_path
}
resource "aws_cloudformation_stack" "deploy_notifications_api" {
  name = "${local.environment_name_slug}-notifications"
  capabilities = [
    "CAPABILITY_IAM",
    "CAPABILITY_AUTO_EXPAND"]
  on_failure = "DELETE"
  depends_on = [
    null_resource.services_npm_recursive_build]
  parameters = {
    AppEnvironment = local.environment_name_slug
    ParamAWSID = var.aws_access_key_id
    ParamAWSKey = var.aws_secret_access_key
    ParamAWSNotificationsSenderEmail = var.notifications_sender_email
    ParamAWSRecipientEmail = var.recipient_email
    ParamAWSRegion = var.aws_default_region
    ParamAWSSNSImmediateNotificationTopic = aws_sns_topic.topic_immediate_notification.arn
    ParamAWSSenderEmail = var.sender_email
    ParamApiVersion = var.api_version_number
    ParamAwsApplicationBaseUrl = aws_s3_bucket.this_public.website_endpoint
    ParamAwsNotificationTableArn = aws_dynamodb_table.this_default_dynamodb_table.arn
    ParamAwsNotificationTableName = aws_dynamodb_table.this_default_dynamodb_table.name
    ParamCognitoAppId = aws_cognito_user_pool_client.this.id
    ParamCognitoPool = aws_cognito_user_pool.this.id
    ParamDBHost = var.rds_db_host
    ParamDBPassword = var.rds_db_password
    ParamDBUsername = var.rds_db_username
    ParamEmailRedirectUrl = "${aws_s3_bucket.this_public.website_endpoint}/sign-up"
    ParamForgotPwdRedirect = "${aws_s3_bucket.this_public.website_endpoint}/reset-password"
    ParamMixPanelToken = var.mix_panel_token
    ParamNotificationRetentionDays = var.notification_retention_days
    ParamS3PrivateBucket = aws_s3_bucket.this_private.id
    ParamS3PublicBucket = aws_s3_bucket.this_assets.id
    ParamVPCPrivateSubnet1 = var.vpc_private_subnet1
    ParamVPCPrivateSubnet2 = var.vpc_private_subnet2
    ParamVPCSG = var.vpc_sg
  }
  template_body = <<STACK
{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Transform": "AWS::Serverless-2016-10-31",
    "Description": "sx-mdc-service-notifications",
    "Globals": {
        "Function": {
            "Timeout": 10,
            "Environment": {
                "Variables": {
                    "API_VERSION": {
                        "Ref": "ParamApiVersion"
                    },
                    "AWS_S3_ACCESS_KEY_ID": {
                        "Ref": "ParamAWSID"
                    },
                    "AWS_S3_SECRET_ACCESS_KEY": {
                        "Ref": "ParamAWSKey"
                    },
                    "AWS_S3_REGION": {
                        "Ref": "ParamAWSRegion"
                    },
                    "AWS_S3_PRIVATE_BUCKET": {
                        "Ref": "ParamS3PrivateBucket"
                    },
                    "AWS_S3_PUBLIC_BUCKET": {
                        "Ref": "ParamS3PublicBucket"
                    },
                    "POSTGRES_HOST": {
                        "Ref": "ParamDBHost"
                    },
                    "POSTGRES_USER": {
                        "Ref": "ParamDBUsername"
                    },
                    "POSTGRES_PWD": {
                        "Ref": "ParamDBPassword"
                    },
                    "PREFIX_COMMON_LIB": "/opt",
                    "MIGRATIONS_DIR": "/opt/rdb-migration/migrations",
                    "COMPANY_REGISTRY_TABLE_NAME": "${aws_dynamodb_table.this_company_registry_table.name}",
                    "ID_TOKEN_TTL": {
                        "Ref": "ParamIdTokenTtl"
                    },
                    "ACCESS_TOKEN_TTL": {
                        "Ref": "ParamAccessTokenTtl"
                    },
                    "REFRESH_TOKEN_TTL": {
                        "Ref": "ParamRefreshTokenTtl"
                    },
                    "AWS_SES_NOTIFICATION_SENDER_EMAIL": {
                        "Ref": "ParamAWSNotificationsSenderEmail"
                    },
                    "AWS_SNS_IMMEDIATE_NOTICATION_TOPIC": {
                        "Ref": "ParamAWSSNSImmediateNotificationTopic"
                    },
                    "AWS_DYNAMO_NOTIFICATION_TABLE": {
                        "Ref": "ParamAwsNotificationTableName"
                    },
                    "MIX_PANEL_TOKEN": {
                        "Ref": "ParamMixPanelToken"
                    },
                    "AWS_APPLICATION_BASE_URL": {
                        "Ref": "ParamAwsApplicationBaseUrl"
                    },
                    "LOG_LEVEL": "info"
                }
            }
        }
    },
    "Parameters": {
        "ParamApiVersion": {
            "Type": "String",
            "Default": "0.0.0"
        },
        "AppEnvironment": {
            "Type": "String"
        },
        "ParamDBHost": {
            "Type": "String"
        },
        "ParamDBUsername": {
            "Type": "String"
        },
        "ParamDBPassword": {
            "Type": "String"
        },
        "ParamCognitoPool": {
            "Type": "String"
        },
        "ParamCognitoAppId": {
            "Type": "String"
        },
        "ParamEmailRedirectUrl": {
            "Type": "String"
        },
        "ParamForgotPwdRedirect": {
            "Type": "String"
        },
        "ParamS3PublicBucket": {
            "Type": "String"
        },
        "ParamS3PrivateBucket": {
            "Type": "String"
        },
        "ParamVPCPrivateSubnet1": {
            "Type": "String"
        },
        "ParamVPCPrivateSubnet2": {
            "Type": "String"
        },
        "ParamVPCSG": {
            "Type": "String"
        },
        "ParamAWSRegion": {
            "Type": "String"
        },
        "ParamAWSID": {
            "Type": "String"
        },
        "ParamAWSKey": {
            "Type": "String"
        },
        "ParamAWSSenderEmail": {
            "Type": "String"
        },
        "ParamAWSRecipientEmail": {
            "Type": "String"
        },
        "ParamAWSNotificationsSenderEmail": {
            "Type": "String"
        },
        "ParamAWSSNSImmediateNotificationTopic": {
            "Type": "String"
        },
        "ParamAwsNotificationTableArn": {
            "Type": "String"
        },
        "ParamAwsNotificationTableName": {
            "Type": "String"
        },
        "ParamAwsApplicationBaseUrl": {
            "Type": "String"
        },
        "ParamNotificationRetentionDays": {
            "Type": "Number"
        },
        "ParamIdTokenTtl": {
            "Type": "String",
            "Default": 1
        },
        "ParamAccessTokenTtl": {
            "Type": "String",
            "Default": 1
        },
        "ParamRefreshTokenTtl": {
            "Type": "String",
            "Default": 30
        },
        "ParamMixPanelToken": {
            "Type": "String"
        }
    },
    "Resources": {
        "MDCNotificationApi": {
            "Type": "AWS::Serverless::Api",
            "Properties": {
                "EndpointConfiguration": "REGIONAL",
                "StageName": "api",
                "Cors": {
                    "AllowOrigin": "'*'",
                    "AllowMethods": "'OPTIONS,HEAD,GET,PUT,POST,PATCH,DELETE'",
                    "AllowHeaders": "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,x-id-token,x-file-path,x-project-id,x-datetime,x-task-id,x-subtask-id,x-vendor-delivery-id,x-prefab-delivery-id,x-project-team-id,x-resource-type,x-resource-id,x-document-id,x-notification-id'"
                }
            }
        },
        "ProcessDynamoDBTrigger": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_notifications_api.key}"
                },
                "Handler": "NotificationEnrichment.handler",
                "Layers": [
                    "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "Stream": {
                        "Type": "DynamoDB",
                        "Properties": {
                            "Stream": "${aws_dynamodb_table.this_default_dynamodb_table.stream_arn}",
                            "BatchSize": 1,
                            "StartingPosition": "LATEST"
                        }
                    }
                },
                "Environment": {
                    "Variables": {
                        "AWS_SES_NOTIFICATION_SENDER_EMAIL": {
                            "Ref": "ParamAWSNotificationsSenderEmail"
                        },
                        "AWS_SNS_IMMEDIATE_NOTIFICATION_TOPIC": {
                            "Ref": "ParamAWSSNSImmediateNotificationTopic"
                        },
                        "AWS_DYNAMO_NOTIFICATION_TABLE": {
                            "Ref": "ParamAwsNotificationTableName"
                        },
                        "MIX_PANEL_TOKEN": {
                            "Ref": "ParamMixPanelToken"
                        },
                        "AWS_APPLICATION_BASE_URL": {
                            "Ref": "ParamAwsApplicationBaseUrl"
                        },
                        "LOG_LEVEL": "info"
                    }
                },
                "Policies": [
                    "AmazonSESFullAccess",
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonDynamoDBFullAccess"
                ]
            }
        },
        "GetNotificationsSummaryByUserEmail": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_notifications_api.key}"
                },
                "Handler": "GetNotificationsSummaryByUserEmail.handler",
                "Layers": [
                    "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetNotificationsSummaryByUserEmail": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/notifications/get-aggregated-notifications",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCNotificationApi"
                            }
                        }
                    }
                },
                "Environment": {
                    "Variables": {
                        "AWS_DYNAMO_NOTIFICATION_TABLE": {
                            "Ref": "ParamAwsNotificationTableName"
                        },
                        "APP_NOTIFICATION_RETENTION_DAYS": {
                            "Ref": "ParamNotificationRetentionDays"
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonDynamoDBFullAccess"
                ]
            }
        },
        "GetNotificationsForProject": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_notifications_api.key}"
                },
                "Handler": "GetNotificationsForProject.handler",
                "Layers": [
                    "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetNotificationCount": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/notifications/get-project-notifications",
                            "Method": "post",
                            "RestApiId": {
                                "Ref": "MDCNotificationApi"
                            }
                        }
                    }
                },
                "Environment": {
                    "Variables": {
                        "AWS_DYNAMO_NOTIFICATION_TABLE": {
                            "Ref": "ParamAwsNotificationTableName"
                        },
                        "APP_NOTIFICATION_RETENTION_DAYS": {
                            "Ref": "ParamNotificationRetentionDays"
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonDynamoDBFullAccess"
                ]
            }
        },
        "DeleteNotificationFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_notifications_api.key}"
                },
                "Handler": "DeleteNotification.handler",
                "Layers": [
                    "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "DeleteNotification": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/notifications/clear-notification",
                            "Method": "delete",
                            "RestApiId": {
                                "Ref": "MDCNotificationApi"
                            }
                        }
                    }
                },
                "Environment": {
                    "Variables": {
                        "AWS_DYNAMO_NOTIFICATION_TABLE": {
                            "Ref": "ParamAwsNotificationTableName"
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonDynamoDBFullAccess"
                ]
            }
        },
        "GetTotalAndClearedNotificationCount": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "CodeUri": {
                    "Bucket": "frontend-dev-bucket-for-sam",
                    "Key": "${aws_s3_bucket_object.file_upload_notifications_api.key}"
                },
                "Handler": "GetTotalAndClearedNotificationCount.handler",
                "Layers": [
                    "${aws_cloudformation_stack.deploy_common_layer.outputs.MDCServiceLayer}"
                ],
                "Runtime": "nodejs12.x",
                "Events": {
                    "GetTotalAndClearedNotificationCount": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/notifications/get-notifications-count",
                            "Method": "get",
                            "RestApiId": {
                                "Ref": "MDCNotificationApi"
                            }
                        }
                    }
                },
                "Environment": {
                    "Variables": {
                        "AWS_DYNAMO_NOTIFICATION_TABLE": {
                            "Ref": "ParamAwsNotificationTableName"
                        },
                        "APP_NOTIFICATION_RETENTION_DAYS": {
                            "Ref": "ParamNotificationRetentionDays"
                        }
                    }
                },
                "Policies": [
                    "AWSLambdaVPCAccessExecutionRole",
                    "AWSLambdaENIManagementAccess",
                    "AmazonDynamoDBFullAccess"
                ]
            }
        }
    },
    "Outputs": {
        "MDCNotificationApi": {
            "Description": "Notifications API endpoint URL",
            "Value": { "Fn::Sub": "https://$${MDCNotificationApi}.execute-api.us-eat-1.amazonaws.com/api/" }

        }
    }
}
STACK
}
